"""
Run the application. CLI parameters override config file parameters
"""

from time import perf_counter
start_time = perf_counter()

import argparse
from datetime import datetime

import forecast.constants as c
import forecast.tasks as tasks


def main():
    # PARSE CLI PARAMETERS
    parser = argparse.ArgumentParser(description='Electricity price forecasting')

    parser.add_argument('--lag', metavar='--lag', type=str, nargs='?', default=0, help='Lag')
    parser.add_argument('--log_level', metavar='--log', type=str, nargs='?', default=c.LOG_LEVEL, help='Log level')
    parser.add_argument('--num_cores', metavar='--n', type=int, nargs='?', default=c.NUM_JOBS, help='number of CPU cores')
    parser.add_argument('--seed', metavar='--s', type=int, nargs='?', default=c.SEED, help='seed for pseudorandom operations')
    parser.add_argument('--sklearn_level', metavar='--sl', type=int, nargs='?', default=c.SKLEARN_LEVEL, help='scikit-learn verbosity')
    parser.add_argument('--task_num', metavar='--t', type=int, nargs='?', default=c.TASK_NUM, help='task number to execute')

    cli_args = parser.parse_args()

    # Start log. Show configuration if debugging
    c.logger.info('===============================================================')
    c.logger.info('Started logging at %s', datetime.now().strftime("%d-%m-%y %H:%M:%S"))
    c.logger.debug('Configuration from config file:')

    if c.logger.getEffectiveLevel() < 20:
        with open(c.LOG_PATH, 'a') as fid:
            DELIMITER = '-' * 35

            # CONFIG FILE CONTENTS
            for section in c.config.sections():
                fid.write(DELIMITER + '\n' + section.rjust(15) + '\n' + DELIMITER + '\n')
                for (key, value) in c.config.items(section):
                    fid.write(key.rjust(15) + ': ' + value + '\n')
                
            # CLI ARGS
            fid.write(DELIMITER + '\n' + 'CLI ARGUMENTS'.rjust(15) + '\n' + DELIMITER + '\n')
            for arg in vars(cli_args):
                fid.write(arg.rjust(15) + ': ' + str(getattr(cli_args, arg)) + '\n')
            
            fid.write(DELIMITER + '\n')


    task_runner = tasks.TaskRunner(cli_args=cli_args)

    execution_time = perf_counter() - start_time
    finish_time = datetime.now().strftime("%d-%m-%y %H:%M:%S")

    c.logger.info('Execution took {} seconds. Finished at:'.format(execution_time, finish_time))
    c.logger.info('===============================================================\n')


if __name__ == '__main__':
    main()

