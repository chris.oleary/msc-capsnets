from time import perf_counter

import pytest


def test_5_benchmark_imports(LIMIT=5):
    """Ensures program loading time is less than specified limit. 
    
    Run before all other tests as Python imports modules once per runtime

    :param LIMIT: number of seconds. Default = 5
    :returns: None
    """

    start = perf_counter()
    import forecast.tasks as tasks
    task = tasks.BenchmarkImports()
    task.run()
    assert perf_counter() - start < LIMIT


def test_1_join_isem_files():
    import forecast.tasks as tasks # imported in function to prevent interference with benchmarking test


def test_2_calculate_performance():
    import forecast.tasks as tasks # imported in function to prevent interference with benchmarking test
    # calculate benchmark scores from K-SVM-SVR model


def test_3_feature_selection():
    import forecast.tasks as tasks # imported in function to prevent interference with benchmarking test
    # check that feature importance scores were run
    # check that RFE scores were run
    # check that plots were created


def test_4_run_models():
    import forecast.tasks as tasks # imported in function to prevent interference with benchmarking test
    # set seeds and check if results are reproducible
