import numpy as np
import pytest

import forecast.util as util

def test_dm_test():
    # Test for the DM test
    # example values from https://rdrr.io/cran/forecast/man/dm.test.html
    e1 = np.array([ 
        0.002241270, -0.000661906, -1.200002613, -4.000218178,  3.799373533,
        3.401005111, -1.199692125, -0.800490048, -1.200048999,  3.399847229,
        7.000708948,  2.200989241, -4.800165569, -1.201043375,  3.400167556,
        3.000709085,  0.400267950, -6.800169207, -2.401259050,  1.200112198,
    ])

    e2 = np.array([
        0.1209998,  8.0622269, -0.9189429, -1.7952436,  5.3431277,  2.0459823,
        -0.6073261,  0.5516816, -0.7633238,  4.1229472,  5.8785333,  1.2828331,
        -3.4854233,  1.1971370,  3.1709314,  2.3408457,  0.4962286, -5.9841362,
        0.1537114,  0.4207951,
    ])

    result = util.dm_test(e1, e2, alternative='two_sided', h=1, power=2)
    tolerance = 0.001
    print(result.dm_stat)
    print(result.p_value)
    assert abs(abs(result.dm_stat) - abs(0.1439)) <= tolerance
    assert abs(abs(result.p_value) - abs(0.8871)) <= tolerance