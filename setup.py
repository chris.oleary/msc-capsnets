from setuptools import setup

with open('READEME.md', 'r') as f:
    long_description = f.read()

setup(
  name='forecast',
  version='0.0.1',
  description='Electricity Price Forecasting Tool',
  license='MIT',
  long_description=long_description,
  author="Christian O'Leary",
  author_email='christian.oleary@cit.com',
  packages=[ 'forecast' ], # same as name
  install_requires=[
    'scikit-learn', 'pandas', 'numpy', 'statsmodels'
  ],
)