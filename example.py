import os
from datetime import datetime
import forecast.tasks as tasks

import tensorflow as tf
my_devices = tf.config.experimental.list_physical_devices(device_type='CPU')
tf.config.experimental.set_visible_devices([], 'GPU')
tf.config.experimental.set_visible_devices(devices=my_devices, device_type='CPU')

if __name__=='__main__':
	# tasks.JoinIsemCsvFiles().run(lag=24*90)
	# tasks.ChannelStatistics().run()

	# tasks.FeatureSelection().run(
	# 	data_path=os.path.join('data', 'dataset-2160.csv'), 
	# 	target='ROI-DA',
	# 	acf_plots=True,
	# 	f_regression=True,
	# 	importance_scores=True,
	# 	pacf_plots=True,
	# 	mi=True,
	# 	multisurf=True,
	# 	rf_rank=True,
	# 	rfe_scores=True,
	# 	)

	scaling = 'minmax'
	for scaling in [None, 'minmax']:#, 'standard', 'robust', 'normalize']:
		tasks.RunModels(
			# use_sklearn=True,
			use_sklearn=False,
			use_keras=True,
			# use_keras=False,
			# use_k_svm_svr=True,
			use_k_svm_svr=False,
			num_iterations=1,
			scaling=scaling,
			search='random',
		).run()

	tasks.CleanResultsFiles().run()
	tasks.GenerateSummary().run()
	tasks.ParameterPerfomanceCorrelation().run()

	print(f'Done at {datetime.now().strftime("%d-%m-%y %H:%M:%S")}')
