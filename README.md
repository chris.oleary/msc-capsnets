The main body of code is in the forecast directory:

* capsnet.py: CapsNet implementation
* constants.py: some application constants and sklearn model setup
* data.py: handles reading and pre-processing data
* k\_svm\_svr.py: My implementation of K-SVM-SVR
* keras\_models.py: NN models implementation
* models.py: Handles running models
* tasks.py: A series of classes that perform specific tasks, e.g. run models, generate summary files, etc.
* util.py: Some miscellaneous functions

## Configuration
CLI parameters override config file parameters. Example use of the code in example.py

## Logging
Logging is done using Python's `logging` module. Logs are located in the logs directory.
Useful commands:
- Linux/cygwin: `tail -f logs/log.log`
- Windows PowerShell: ``

## CUDA
Different instances of same program can be run using different GPUS with:
`CUDA_VISIBLE_DEVICES=0 python ...`
`CUDA_VISIBLE_DEVICES=1 python ...`