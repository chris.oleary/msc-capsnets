from time import perf_counter

import numpy as np
from sklearn.cluster import KMeans
from sklearn.svm import SVC, SVR

import forecast.constants as c

class KSvmSvr():

    search_space = {
        # K-Means
        # 'n_clusters': [2, 3, 4, 5],
        'n_clusters': [3],
        # SVC
        'svc_kernel': ['rbf'],
        'svc_gamma': ['scale', 'auto'], 
        'svc_tol': [1e-3, 1e-4, 1e-5],
        'svc_C': [0.01, 0.1, 1.0, 10],
        'svc_epsilon': [0.0, 0.5, 1.0],
        'svc_shrinking': [True, False],
        # SVR
        # 'svr_kernel': ['linear', 'poly', 'rbf', 'sigmoid'],
        # 'svr_kernel': ['rbf', 'sigmoid'],
        'svr_kernel': ['rbf'],
        # 'svr_degree': [2, 3, 4, 5, 6],
        'svr_degree': [2, 4],
        # 'svr_gamma': ['scale', 'auto'], 
        'svr_gamma': ['scale'], 
        # 'svr_gamma': [.02,.0374,.04,.05], 
        'svr_coef0': [0.0, 0.5, 1.0, 2.0, 5.0],
        'svr_tol': [1e-3, 1e-4, 1e-5],
        'svr_C': [0.01, 0.1, 1.0, 10],
        # 'svr_C': [15,60,100,64,55,45,70],
        'svr_epsilon': [0.0, 0.5, 1.0],
        'svr_shrinking': [True, False],
        'svr_max_iter': [100000],
    }

    def __init__(self, **kwargs):
        self.params = {
            'n_clusters': 3,
            'random_state': 0,
            'svm_kernel': 'rbf',
            'n_jobs': 1
        }
        self.set_params(**kwargs)
        self.multiple_clusters = True
    

    def fit(self, X, y, **kwargs):
        # 1. cluster data
        c.logger.debug('Fitting K-Means with cluster size {self.params["n_clusters"]}...')
        start = perf_counter()
        for attempt in range(5):
            kmeans = KMeans(
                n_clusters=self.params['n_clusters'], 
                random_state=self.params['random_state'] + attempt,
                n_jobs=self.params['n_jobs'],
                ).fit(X)
            
            num_clusters = np.unique(kmeans.labels_).shape[0]
            if num_clusters == self.params['n_clusters']:
                break
            else:
                c.logger.debug(f'Failed to create the required number of clusters ({self.params["n_clusters"]})')
                print('Fail')

        print('n_clusters', self.params['n_clusters'])
        if num_clusters < 2:
            raise ValueError('Failed to create multiple clusters')
        elif num_clusters != self.params['n_clusters']:
            c.logger.debug(f'Failed to create the required number of clusters. Using {num_clusters} instead')
            print(f'Failed to create the required number of clusters. Using {num_clusters} instead')
            self.params['n_clusters'] = num_clusters
        
        c.logger.debug(f'Took {round(perf_counter() - start, 2)} seconds')

        cluster_sizes = np.unique(kmeans.labels_, return_counts=True)
        print('cluster_sizes', cluster_sizes)
        formatted_sizes = [ 
            f'{row[0]}: {row[1]} instances' for row in 
            np.asarray(cluster_sizes).T.tolist() 
            ]
        c.logger.debug(f'K-Means cluster sizes: {formatted_sizes}')

        # 2. train a classifier on data with RBF kernel
        c.logger.debug('Fitting SVC...')
        start = perf_counter()
        self.svm = SVC(
            kernel='rbf', 
            random_state=self.params['random_state'],
            max_iter=100000,
            )
        self.svm.fit(X, kmeans.labels_)
        c.logger.debug(f'Took {round(perf_counter() - start, 2)} seconds')
        
        # 3. train k regression models
        c.logger.debug('Fitting SVR...')
        start = perf_counter()
        
        svr_params = { k[4:]: v for k, v in self.params.items() if 'svr_' in k}
        self.svr_models = []
        df = X[list(X.columns)]
        df['cluster'] = kmeans.labels_
        df['y'] = y
        
        for i in range(self.params['n_clusters']):
            cluster = df[df['cluster'] == i] # get instances of cluster
            y = cluster['y'] # get corresponding labels
            features = cluster.drop(['cluster', 'y'], axis=1) # get corresponding features
            self.svr_models.append(SVR(**svr_params).fit(features, y)) # Fit a model and add to list

        df = df.drop('cluster', axis=1)
        df = df.drop('y', axis=1)
        c.logger.debug(f'Took {round(perf_counter() - start, 2)} seconds')
        
    
    def predict(self, X):
        # 1. Use SVM to select a regression model for each instance
        X['cluster'] = self.svm.predict(X)

        # 2. For each SVR model forecast for relevant instances
        predictions = []
        for cluster in range(self.params['n_clusters']):
            df = X[X['cluster'] == cluster]
            df = df.drop('cluster', axis=1)
            if df.shape[0] > 0:
                predictions.append(self.svr_models[cluster].predict(df))
        
        predictions = np.concatenate(predictions, axis=None)
        return predictions
    

    def set_params(self, **kwargs):
        for key, value in kwargs.items():
            self.params[key] = value
        return self.params
    

    def get_params(self):
        return self.params
    
