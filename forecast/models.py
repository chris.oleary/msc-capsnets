import gc
import os
import random
from time import perf_counter

import numpy as np
import pandas as pd
from sklearn.model_selection import ParameterGrid
import tensorflow as tf
import tensorflow.keras.backend as K

import forecast.constants as c
from forecast.keras_models import architectures, ModelHandler, search_space
from forecast.k_svm_svr import KSvmSvr
import forecast.util as util

class ModelRunner():
	"""Instantiates and runs models"""

	def __init__(self):
		"""Set some default parameters"""

		self.params = {}


	def train_ml_models(self, data, **kwargs):
		"""Instantiate and run models on a dataset
			
		:param data: dataframe to use
        :param parameters: model agnostic parameters
		:param kwargs: optional arguments
		:returns: list of dicts where each dict contrains the results of running a type of model
		"""

		self.params = kwargs.copy()

		# 1. Train sklearn models
		if kwargs.get('use_sklearn', True):
			for name, func in c.sklearn_models.items():
				c.logger.debug('Training model: ' + name)
				self.train_ml_model(name, func, data, c.sklearn_search_space[name], **kwargs)

		# 2. Train keras models
		if kwargs.get('use_keras', True):
			for name, arch in architectures.items():
				c.logger.debug('Training model: ' + name)
				self.train_ml_model(name, ModelHandler.build_model, data, search_space, **kwargs)

		# 3. Train K-SVM-SVR model
		if kwargs.get('use_k_svm_svr', True):
			c.logger.debug('Training model: K-SVM-SVR')
			self.train_ml_model('k_svm_svr', KSvmSvr, data, KSvmSvr.search_space, **kwargs)


	def train_ml_model(self, model_name, model_func, data, search_space, **kwargs):
		"""Train a model on the provided data and make predictions
		
		:param model_name: the name of a model type
		:param model_func: a function to construct a model.
		:param data: a tuple of six elements (X_train, X_val, X_test, y_train, y_val, y_test)
		:param kwargs: keyword arguments
		:returns: 
		"""

		self.params = kwargs.copy()
		arch = kwargs.get('arch', None)
		search = kwargs.get('search', None)
		validate_by_day = kwargs.get('validate_by_day', c.validate_by_day)

		param_options = self._get_param_options(model_name, search_space, search) # list of model parameter combinations
		X_train, X_val, X_test, X_train_val, X, y_train, y_val, y_train_val, y_test, y = data

		# Stack list on days into one dataframe
		X = pd.concat(X)
		X_train = pd.concat(X_train)
		X_train_val = pd.concat(X_train_val)

		y_train = pd.concat(y_train)
		y_val = pd.concat(y_val)
		y_train_val = pd.concat(y_train_val)
		y_test = pd.concat(y_test)
		y = pd.concat(y)

		size = len(param_options)
		count = 0


		for params in param_options:
			start_time = perf_counter()
			num_fails = 0
			# try:
			# create model
			print(model_name)
			if model_name in architectures:
				model = model_func(input_dim=X_train.shape[1], arch=model_name, **params)
			else:
				model = model_func(**params)
			
				# if model_name != 'adaboost': 
				# 	model.set_params(**params)
			
			try: # some models do not have this parameter and may be deterministic
				model.set_params(**params)
				model.set_params(random_state=c.SEED)
				print('FAIL TO SET PARAMS')
			except:
				if count == 0: # only log warning on first iteration
					c.logger.debug('(cannot set random_state of model ' + model_name + ')')

			# Fit on training data and pre-test on validation data
			if model_name in architectures and not validate_by_day:
				model.fit(X_train_val, y_train_val, val_split=c.val_split)
				y_val_pred = model.predict(X_val) # approximation
			else:
				model.fit(X_train, y_train)
				median = np.median(y_train)
				y_val_pred, num_fails = self._run_daily_predictions(model, X_val, median=median)
				
				# Fit model on training *and* validation data before final testing
				model.fit(X_train_val, y_train_val)

			# Test model on test data
			median = np.median(y_train_val)
			y_test_pred, num_fails = self._run_daily_predictions(model, X_test, median=median)

			# Get run time and scores
			run_time = perf_counter() - start_time
			val_scores = util.get_scores(y_val, y_val_pred, prefix='val_')
			test_scores = util.get_scores(y_test, y_test_pred, prefix='test_')

			# except Exception as e:
			# 	 c.logger.warning('Failed to run train model ' + model_name)
			# 	 c.logger.error(str(e))
			# 	 run_time = perf_counter() - start_time
			# 	 val_scores = { 'val_' + metric: 'FAIL' for metric in list(util.metrics.keys()) }
			# 	 test_scores = { 'test_' + metric: 'FAIL' for metric in list(util.metrics.keys()) }
			
			self.save_results(model, model_name, X, val_scores, test_scores, run_time, num_fails, **kwargs)

			del model
			gc.collect()
			K.clear_session()
			tf.compat.v1.reset_default_graph() # TF graph isn't same as Keras graph


	def save_results(self, model, model_name, X, val_scores, test_scores, run_time, num_fails, **kwargs):

		# Save model predictions
		predictions_path = os.path.join(os.sep.join(kwargs['results_path'].split(os.sep)[:-1]), \
			f'predictions_{model_name}.csv')
		
		save_predictions = kwargs.get('save_predictions', c.SAVE_PREDICTIONS)
		if save_predictions and not os.path.exists(predictions_path):
			try:
				model.fit(X, y)
				preds = model.predict(X)
				scores = util.get_scores(y, preds)
				for k,v in scores.items():
					print(f'{k}: {v}')
				np.savetxt(predictions_path, preds, delimiter=',')
			except:
				# print(f'Unable to save save predictions for model: {model_name}')
				c.logger.error(f'Unable to save save predictions for model: {model_name}')

		results = { 
			'model': model_name, 
			'seed': c.SEED,
			**val_scores, 
			**test_scores, 
			**self.params,
			'time': run_time, 
			'num_fails': num_fails,
		}

		if kwargs['results_path'] != None:
			# Write scores to a results file where models are compared
			util.results_to_file(results, kwargs['results_path'])

			# Write scores and parameters to a results file for a type of model
			scores_and_params = { **model.get_params(), **results }

			for base_name in ['base_estimator', 'estimator']:
				if base_name in scores_and_params.keys():				
					scores_and_params[base_name] = type(scores_and_params[base_name]).__name__
				# scores_and_params = { k.replace('estimator__', ''): v for k, v in scores_and_params.items() }

			util.results_to_file(scores_and_params, os.path.join(c.RESULTS_DIR, model_name + '.csv'))


	def train_stats_models(self, data, parameters, **kwargs):
		raise NotImplementedError()
		# # Fit statistical models
		# if use_stats:
		#     for name, func in c.multivariate_stats_models.items():
		#         c.logger.debug('Fitting model: ' + name)
		#         self.train_stats_model(data, name, func, results_path)


	def train_stats_model(self, data, model_name, model_func, results_path, target='ROI-DA'):
		"""Trains a statistical model on data.
		
		:param data: train set and test set in a tuple
		:param model_name: name of model
		:param model_func: function to instantiate the model
		:param results_path: path to results file
		:param target: text of target column name
		"""
        
		start_time = perf_counter()
		train, test = data
		model = model_func(data=train) # create model
		model_fit = model.fit()
		predictions = model_fit.forecast(model_fit.y, steps=test.shape[0])
		y_pred = predictions[:, 1]
		y_test = test[target]

		run_time = perf_counter() - start_time
		results = { 'model': model_name, **util.get_scores(y_test, y_pred), 'time': run_time }
		util.results_to_file({ **results, **self.params }, results_path)


	def _get_param_options(self, model_name, search_space, method='random', random_search_limit=60):
		"""Generate a list of model parameter combinations
        
		:param model_name: the model name (str)
		:param search_space: a dict of lists of parameter options
		:param method: the generation method ('grid', 'random' or None)
		:random_search_limit: number of combinations generated if using random method
		"""

		if method == None:
			param_options = [{}]
		elif method == 'grid':
			# TODO: remove redundant/illegal parameter combinations
			param_options = list(ParameterGrid(search_space))
		elif method == 'random':
			param_options = [self._random_params(search_space) for i in range(random_search_limit)]
		else:
			raise ValueError('Unknown parameter search method:', method)

		return param_options


	def _random_params(self, search_space):
		"""Select parameters randomly for a model
        
		:param search_space: The search space options of each model
		:returns: the selectied model parameters
		"""

		param_selection = {}
		for i, param in enumerate(search_space): # for each model parameter
			param_values = search_space[param] # list of grid search values for parameter
            
			if len(param_values) > 0:
				if isinstance(param_values[0], float): # if float select any real number
					param_selection[param] = random.SystemRandom(c.SEED+i).uniform(min(param_values), max(param_values))
				else: # otherwise choose from grid search list
					param_selection[param] = random.SystemRandom(c.SEED+i).choice(param_values)

		return param_selection


	def _run_daily_predictions(self, model, days, median=None, target_col='ROI-DA'):
		"""Runs a model on 24 predictions at a time, backfilling prices from ongoing predictons"""

		y_pred = list()
		num_fails = 0

		lag_cols = {}
		for col in list(days[0].columns): # for each column
			lag = int(col.split('-')[-1])
			if lag < 24 and target_col in col: # if it contains target data for predicted day that could be unavailable
				lag_cols[col] = lag # record its associated lag

		for day in days:
			day = day.copy(deep=True)
			# day.to_csv('before.csv')
			for hour in range(day.shape[0]):
				period = day.iloc[[hour]]
				try:
					prediction = model.predict(period)
					if median != None and (not np.isfinite(prediction[0]) or np.isnan(prediction[0])):
						num_fails += 1
						prediction = median

				except Exception as e:
					print(f'Failed to predict for hour {hour}...')
					print(period)
					raise ValueError(e)

				y_pred.append(prediction)

				# propogate predictions
				for period in range(hour, day.shape[0]): # for each remaining period
					for col, lag in lag_cols.items(): # for each of the relevant cols
						if len(y_pred) >= lag:
							day.at[day.index[period], col] = y_pred[-lag]
			# day.to_csv('after.csv')

		result_vector = np.array(y_pred).flatten()
		return result_vector, num_fails

