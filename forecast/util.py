import csv
import collections
import math
import os
from time import perf_counter, sleep

import numpy as np
import pandas as pd
from scipy.stats import spearmanr, t
from sklearn.metrics import mean_absolute_error, mean_squared_error, median_absolute_error, r2_score
from statsmodels.tsa.stattools import acovf

import forecast.constants as c

s = 0.0000000001 # smoothing constant to prevent division by zero for MAPE
metrics = {
    'MAE': lambda y_true, y_pred: mean_absolute_error(y_true, y_pred), 
    'MAE2': lambda y_true, y_pred: median_absolute_error(y_true, y_pred), 
    'MAPE': lambda y_true, y_pred: np.mean(np.abs((y_true - y_pred) / (y_true + s))) * 100, 
    'ME': lambda y_true, y_pred: np.mean(y_true - y_pred),
    'MSE': lambda y_true, y_pred: mean_squared_error(y_true, y_pred),
    'R2': lambda y_true, y_pred: r2_score(y_true, y_pred),
    'RMSE': lambda y_true, y_pred: math.sqrt(mean_squared_error(y_true, y_pred)),
    'SRC correlation': lambda y_true, y_pred: spearmanr(y_true, y_pred).correlation,
    'SRC p value': lambda y_true, y_pred: spearmanr(y_true, y_pred).pvalue,
}

def timer(func):
    """Decorator to time functions"""

    def wrap(*args):
        start = perf_counter()
        func(*args)
        print('Total time:', round(perf_counter() - start, 3), 'seconds')
    return wrap


def get_scores(y_true, y_pred, prefix):
    """Calculate error metrics: MAE, MAPE, ME, MSE, R2, RMSE, SRC
    
    :param y_true: actual time series values
    :param y_pred: predicted time series values
    :param prefix: prefix to add to score
    :returns: a dictionary of scores
    """

    scores = {}
    for metric, score_func in metrics.items():
        score = score_func(y_true, y_pred)
        if not np.isfinite(score) or np.isnan(score):
            score = -float('inf') # placeholder
        scores[str(prefix) + metric] = score
    return scores


def dm_test(e1, e2, alternative='two_sided', h=1, power=2):
    """Diebold Mariano test for determinining if forecasts are statistically different
    
    :param e1: forecast errors from the first method
    :param e2: forecast errors from the second method
    :param alternative: str specifying the alternative hypothesis, 'two_sided' (default one), 'less' or 'greater'
    :param h: forcasting horizon used in calculating errors (e1, e2)
    :param power: power used in the loss function (usually 1 or 2)
    :return: named tuple containing DM statistic and p-value
    """

    alternatives = ['two_sided', 'less', 'greater']
    if alternative not in alternatives:
        raise ValueError(f"alternative must be one of {alternatives}")

    d = np.abs(e1) ** power - np.abs(e2) ** power
    n = d.shape[0]
    d_cov = acovf(d, fft=True, nlag=h-1)
    d_var = (d_cov[0] + 2 * d_cov[1:].sum()) / n

    if d_var > 0:
        dm_stat = np.mean(d) / np.sqrt(d_var)
    elif h == 1:
        c.logger.error('Variance of DM statistic is zero')
        raise ValueError('Variance of DM statistic is zero')
    else:
        c.logger.warning('Variance is negative, using horizon h=1')
        return dm_test(e1, e2, alternative=alternative, h=1, power=power)

    # The corrected statistic suggested by HLN
    k = ((n + 1 - 2 * h + h / n * (h - 1)) / n) ** 0.5
    dm_stat *= k

    if alternative == 'two_sided':
        p_value = 2 * t.cdf(-abs(dm_stat), df=n - 1)
    else:
        p_value = t.cdf(dm_stat, df=n - 1)
        if alternative == 'greater':
            p_value = 1 - p_value

    dm_test_result = collections.namedtuple('dm_test_result', ['dm_stat', 'p_value'])
    return dm_test_result(dm_stat=dm_stat, p_value=p_value)


def create_dir(path):
    """Ensure a directory path exists. Ignores any filename specified at the end of the path
    
    :param path: path which may or may not include a file name
    :returns: None
    """

    if path != None and not os.path.exists(path):
        current_path = ''
        for part in path.split(os.sep):
            current_path += part + os.sep
            if not os.path.exists(current_path) and '.' not in current_path:
                os.makedirs(current_path)


def results_to_file(results, path):
    """Record modelling results in a CSV file.
    
    :param list results: a dict containing results from running a model
    :param str path: the result file path
    :returns: None
    """

    if len(results) > 0:
        HEADERS = sorted(list(results.keys()), key=lambda v: v.upper())
        if 'model' in HEADERS:
            HEADERS.insert(0, HEADERS.pop(HEADERS.index('model')))

        for key, value in results.items():
            if value == None or value == '':
                results[key] = 'None'

        try:
            _write_to_csv(path, results, HEADERS)
        except:
            # try a second time: sometimes permission error is due to Python not 
            # having closed the file fast enough after the previous write
            sleep(1) # in seconds
            _write_to_csv(path, results, HEADERS)

    else:
        raise ValueError('Empty results dict')


def _write_to_csv(path, results, headers):
    """Open and write results to CSV file.
    
    :param path: path to file
    :param results: dict of values to write
    :param headers: list of strings to order values by
    """

    # Create directories if they do not exist
    create_dir(path)
    is_new_file = not os.path.exists(path)

    with open(path, 'a+', newline='') as f:
        writer = csv.writer(f, delimiter=',')
        if is_new_file:
            writer.writerow(headers)
        writer.writerow([results[header] for header in headers])


def isNewCombination(self, parameters, path):
    """Checks to see if a parameter combination has already been tried.
    
    :param parameters: the parameters of interest
    :param path: the results file path
    :returns: True if the parameter combination has not been recorded yet, False otherwise
    """

    if not os.path.exists(path):
        return True
    
    results = pd.read_csv(path).astype(str)
    for key in parameters.keys():
        results = results.loc[results[key] == str(parameters[key])]
    return results.shape[0] == 0


def check_intervals(df, col='ROI-DA'):  
    """Check for gaps in a column of a time series
    
    :param df: dataframe
    :param col: [DEPRECATED]
    :returns: the unaltered dataframe
    """

    # is_gap = (df.index.to_series().diff() / pd.Timedelta('1 hour')) > 1
    # num_gaps = df[is_gap == True].shape
    for col in df.columns:
        num_gaps = df[col].isna().sum()
        c.logger.debug(('Number of missing values in ' + str(col) + ' -> ' + str(num_gaps)))
    return df
