"""
Adapted from multiple sources including:
1. Squash function structure: https://github.com/ageron/handson-ml/blob/master/extra_capsnets.ipynb
2. Most of code: https://github.com/XifengGuo/CapsNet-Keras
3. Fixes from: https://github.com/XifengGuo/CapsNet-Keras/issues/98
"""

import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras import initializers, layers


class Length(layers.Layer):
    """
    Compute the length of vectors. Used to compute a Tensor that has the same shape with y_true in margin_loss.
    Using this layer as model's output can directly predict labels by using `y_pred = np.argmax(model.predict(x), 1)`
    inputs: shape=[None, num_vectors, dim_vector]
    output: shape=[None, num_vectors]
    """

    def call(self, inputs, **kwargs):
        output = K.sqrt(K.sum(K.square(inputs), -1) + K.epsilon())
        # print('Length output.shape', output.shape)
        return output


    def compute_output_shape(self, input_shape):
        return input_shape[:-1]


    def get_config(self):
        config = super(Length, self).get_config()
        return config


class Mask(layers.Layer):
    """
    Mask a Tensor with shape=[None, num_capsule, dim_vector] either by the capsule with max length or by an additional 
    input mask. Except the max-length capsule (or specified capsule), all vectors are masked to zeros. Then flatten the
    masked Tensor.
    For example:
        ```
        x = keras.layers.Input(shape=[8, 3, 2])  # batch_size=8, each sample contains 3 capsules with dim_vector=2
        y = keras.layers.Input(shape=[8, 3])  # True labels. 8 samples, 3 classes, one-hot coding.
        out = Mask()(x)  # out.shape=[8, 6]
        # or
        out2 = Mask()([x, y])  # out2.shape=[8,6]. Masked with true labels y. Of course y can also be manipulated.
        ```
    """

    def call(self, inputs, **kwargs):
        if type(inputs) is list:  # true label is provided with shape = [None, n_classes], i.e. one-hot code.
            assert len(inputs) == 2
            inputs, mask = inputs
        else:  # if no true label, mask by the max length of capsules. Mainly used for prediction
            # compute lengths of capsules
            x = K.sqrt(K.sum(K.square(inputs), -1))
            # generate the mask which is a one-hot code.
            # mask.shape=[None, n_classes]=[None, num_capsule]
            mask = K.one_hot(indices=K.argmax(x, 1), num_classes=x.get_shape().as_list()[1])

        # inputs.shape=[None, num_capsule, dim_capsule]
        # mask.shape=[None, num_capsule]
        # masked.shape=[None, num_capsule * dim_capsule]
        masked = K.batch_flatten(inputs * K.expand_dims(mask, -1))
        return masked


    def compute_output_shape(self, input_shape):
        if type(input_shape[0]) is tuple:  # true label provided
            return tuple([None, input_shape[0][1] * input_shape[0][2]])
        else:  # no true label provided
            return tuple([None, input_shape[1] * input_shape[2]])


    def get_config(self):
        config = super(Mask, self).get_config()
        return config


def squash(input_vectors, axis=-1, zoom=1.0):
    """
    The non-linear activation used in Capsule. It drives the length of a large vector to near 1 and small vector to 0

    Introduced a parameter: zoom. If 0.5, the norm will be zoomed in while original norm is less than 0.5. if 0.5, 
    the norm will be zoomed in while original norm is less than 0.5 and be zoomed out while original norm is greater 
    than 0.5.
    
    :param input_vectors: vectors to be squashed, N-dim tensor
    :param axis: axis to squash
    :param zoom: affects vector norm
    :return: tensor with same shape as input vectors
    """

    s_squared_norm = K.sum(K.square(input_vectors), axis, keepdims=True) # ||s||^2
    safe_norm = K.sqrt(s_squared_norm + K.epsilon()) # ~ ||s||
    squash_factor = s_squared_norm / (zoom + s_squared_norm) # ||s||^2 / (1 + ||s||^2)
    unit_vector = input_vectors / safe_norm # s / ||s||

    return squash_factor * unit_vector


class CapsuleLayer(layers.Layer):
    """
    The capsule layer. It is similar to Dense layer. Dense layer has `in_num` inputs, each is a scalar, the output of the 
    neuron from the former layer, and it has `out_num` output neurons. CapsuleLayer just expand the output of the neuron
    from scalar to vector. So its input shape = [None, input_num_capsule, input_dim_capsule] and output shape = \
    [None, num_capsule, dim_capsule]. For Dense Layer, input_dim_capsule = dim_capsule = 1.
    
    :param num_capsule: number of capsules in this layer
    :param dim_capsule: dimension of the output vectors of the capsules in this layer
    :param routings: number of iterations for the routing algorithm
    """

    def __init__(self, num_capsule, dim_capsule, routings=3,
                kernel_initializer='glorot_uniform',
                **kwargs):
        super(CapsuleLayer, self).__init__(**kwargs)
        self.num_capsule = num_capsule
        self.dim_capsule = dim_capsule
        self.routings = routings
        self.kernel_initializer = initializers.get(kernel_initializer)


    def build(self, input_shape):
        assert len(input_shape) >= 3, 'The input Tensor should have shape=[None, input_num_capsule, input_dim_capsule]'
        assert input_shape[1] != None, 'Missing input_num_capsule (got None)'
        self.input_num_capsule = input_shape[1]
        self.input_dim_capsule = input_shape[2]
        # print('input_shape', input_shape)
        # print([self.num_capsule, self.input_num_capsule, self.dim_capsule, self.input_dim_capsule])
        # Transform matrix
        self.W = self.add_weight(shape=[self.num_capsule, self.input_num_capsule,
                                        self.dim_capsule, self.input_dim_capsule],
                                initializer=self.kernel_initializer,
                                name='W')

        self.built = True


    def call(self, inputs, training=None):
        # print('inputs [None, 32, 8]', inputs.shape)

        # inputs_expand = K.expand_dims(inputs, 1) # original
        inputs_expand = tf.expand_dims(inputs, 1) # fix
        # print('inputs_expand [None, 1, 32, 8]', inputs_expand.shape)

        # inputs_tiled = K.tile(inputs_expand, [1, self.num_capsule, 1, 1]) # original
        inputs_tiled = tf.tile(inputs_expand, [1, self.num_capsule, 1, 1]) # fix
        inputs_tiled = tf.expand_dims(inputs_tiled, 4)
        # print('inputs_tiled [None, 1, 32, 8, 1]', inputs_tiled.shape)

        # print('W [1, 32, 16, 8]', self.W.shape)
        # inputs_hat = K.map_fn(lambda x: K.batch_dot(x, self.W, [2, 3]), elems=inputs_tiled) # original
        inputs_hat = tf.map_fn(lambda x: tf.matmul(self.W, x), elems=inputs_tiled) # fix
        # print('inputs_hat [None, 1, 32, 16, 1]', inputs_hat.shape)

        # Routing algorithm
        # b = tf.zeros(shape=[K.shape(inputs_hat)[0], self.num_capsule, self.input_num_capsule]) # original
        b = tf.zeros(shape=[tf.shape(inputs_hat)[0], self.num_capsule, self.input_num_capsule, 1, 1]) # fix
        # print('b [None, 1, 32, 1, 1]', b.shape)

        assert self.routings > 0, 'The routings should be > 0.'
        for i in range(self.routings):
            # c = tf.nn.softmax(b, dim=1) # original
            # c = tf.keras.activations.softmax(b, axis=-1) # updated Keras
            c = layers.Softmax(axis=1)(b) # Fix
            # print('c [None, 1, 32, 1, 1]', c.shape)

            # outputs = squash(K.batch_dot(c, inputs_hat, [2, 2]))  # original
            outputs = tf.multiply(c, inputs_hat) # Fix
            outputs = tf.reduce_sum(outputs, axis=2, keepdims=True)
            outputs = squash(outputs, axis=-2)
            # outputs = squash(tf.reduce_sum(c * inputs_hat, axis=1, keepdims=True)) # other fix?
            # print('outputs [None, 1, 1, 16, 1]', outputs.shape)

            if i < self.routings - 1:
                # b += K.batch_dot(outputs, inputs_hat, [2, 3]) # original
                outputs_tiled = tf.tile(outputs, [1, 1, self.input_num_capsule, 1, 1]) # Fix
                agreement = tf.matmul(inputs_hat, outputs_tiled, transpose_a=True)
                b = tf.add(b, agreement)

        outputs = tf.squeeze(outputs, [2, 4])
        # print('outputs [None, 1, 16]', outputs.shape, '\n\n')
        return outputs


    def compute_output_shape(self, input_shape):
        return tuple([None, self.num_capsule, self.dim_capsule])


    def get_config(self):
        config = {
            'num_capsule': self.num_capsule,
            'dim_capsule': self.dim_capsule,
            'routings': self.routings
        }
        base_config = super(CapsuleLayer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


def PrimaryCap(prev_layer, n_channels, dim_capsule, kernel_size):#, strides, padding):
    """
    Apply Conv2D `n_channels` times and concatenate all capsules
    :param n_channels: the number of types of capsules
    :param dim_capsule: the dim of the output vector of capsule
    :return: output tensor, shape=[None, num_capsule, dim_capsule]
    """

    conv = layers.Conv1D(filters=n_channels*dim_capsule, kernel_size=kernel_size)#(prev_layer)
    reshaped = layers.Reshape(target_shape=[-1, dim_capsule])#(conv)
    squashed = layers.Lambda(squash)#(reshaped)
    return squashed

