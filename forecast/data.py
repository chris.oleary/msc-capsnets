'''
@author Christian O'Leary

Code to handle importing and splitting of data
'''

import os
import pandas as pd
import random

# from imblearn.combine import SMOTETomek, SMOTEENN
# from imblearn.over_sampling import RandomOverSampler, SMOTE, ADASYN
# from imblearn.under_sampling import RandomUnderSampler
import numpy as np
# from sklearn_relief import RReliefF
# from skrebate import MultiSURF, ReliefF
from sklearn.feature_selection import f_regression, mutual_info_regression, SelectKBest
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, Normalizer, RobustScaler, StandardScaler


import forecast.constants as c

class DataHandler():
	'''Class to read data into dataframes and split/shuffle accordingly'''

	def __init__(self):
		self.PLACEHOLDER = -10000.0


	def read_data(self, input_path, impute=True, drop_recent_features=True):
		'''Read data from a file into a dataframe'''
		if not os.path.isfile(input_path):
			raise OSError('File not found', input_path)

		extension = input_path.split('.')[-1]
		if extension == 'csv':
			df = pd.read_csv(input_path, index_col=0)
		elif extension == 'xlsx':
			df = pd.read_xlsx(input_path, index_col=0)
		else:
			raise OSError('Unexpected file type', input_path)

		if impute:
			df = self.impute(df)

		if drop_recent_features:
			df = self.drop_recent_features(df)
		return df


	def split_data(self, X, y=None, **kwargs):
		"""Divide a dataset into training, validation and test datasets"""

		# 1. Divide data by day
		num_periods = kwargs.get('num_periods', 24) # number of prices in day
		days_X, days_y = self._data_by_days(X, y, num_periods)
    
		# 2. Divide days into datasets (also apply scaling, resampling)
		X_train, X_val, X_train_val, X_test, X, y_train, y_val, y_train_val, y_test, y = self._split_dataset(days_X, days_y, **kwargs)

		c.logger.debug(f'# Days: X_train: {len(X_train)}, X_val: {len(X_val)}, X_test: {len(X_test)}, y_train: \
			{len(y_train)}, y_val: {len(y_val)}, y_test: {len(y_test)}')
		data = (X_train, X_val, X_test, X_train_val, X, y_train, y_val, y_train_val, y_test, y)
		return data


	def _data_by_days(self, X, y, num_periods=24):
		"""Divides data into lists of dataframes and makes assertions about length and shapes.
    
		Arguments:
			X {pd.DataFrame} -- feature matrix
			y {pd.DataFrame} -- target vector
    
		Keyword Arguments:
			num_periods {int} -- number of periods per dataframe (default: {24})
    
		Returns:
			two equal length lists of dataframes
		"""

		assert np.isnan(X.values.any()) == False
		assert np.isfinite(X.values.all())

		# find start times for each day
		days_X = np.array_split(X, X.shape[0] / num_periods)
		for day in days_X:
			assert day.index[0].split()[1][:2] == str(23) # starts at correct hour
			assert day.index[-1].split()[1][:2] == str(22) # ends at correct hour
			assert day.shape[0] == num_periods # has correct number of periods
      
		try:
			days_y = np.array_split(y, y.shape[0] / num_periods)
			for day in days_y:
				assert day.index[0].split()[1][:2] == str(23) # starts at correct hour
				assert day.index[-1].split()[1][:2] == str(22) # ends at correct hour
				assert day.shape[0] == num_periods

			assert len(days_X) == len(days_y)
			return days_X, days_y
		except:
			# TODO: handle where y is None
			return days_X


	def isolate_targets(self, df, **kwargs):

		# Get target columns
		target = kwargs['target']
		num_outputs = kwargs['num_outputs']
		target_cols = [ f'{target}-{i}' for i in range(1, num_outputs) ]
		target_cols.insert(0, target)

		# Seperate targets and features
		y = df[target_cols]
		X = df.drop(target_cols, axis=1)
		return X, y


	def _split_dataset(self, days_X, days_y, **kwargs):
		"""Splits a dataset by day, month or year.
    
		Arguments:
			days_X {list} -- features
			days_y {list} -- target variable
			test_split {float} -- percentage of the dataset to use for testing
			val_split {float} -- percentage of dataset to use for validation
			seed {int} -- seed for reproducibility
    
		Returns:
			data -- tuple
		"""

		scaling = kwargs.get('scaling', None)
		seed = kwargs.get('seed', c.SEED)
		test_split = kwargs.get('test_split', 0.1)
		val_split = kwargs.get('val_split', 0.1)
		c.logger.debug(f'Test split: {test_split}. Validation split: {val_split}.')

		# 1. get list of indices and shuffle
		indices = list(range(len(days_X)))
		random.SystemRandom(seed).shuffle(indices)

		# 2. Isolate test data
		test_indices = indices[ : int(len(indices) * test_split) ]
		X_test = [days_X[i] for i in test_indices]
		y_test = [days_y[i] for i in test_indices]

		# 3. Get training and validation data
		train_val_indices = indices[ int(len(indices) * test_split) : ]
		X_train_val = [days_X[i] for i in train_val_indices]
		y_train_val = [days_y[i] for i in train_val_indices]

		# 4. Apply scaling
		c.logger.debug(f'Appling scaling: {scaling}')
		datasets = self.scale_data(scaling, {'X_train_val': X_train_val, 'X_test': X_test, 'days_X': days_X})
		X_test = datasets['X_test']

		# 5. Seperate training and validation data
		val_indices = indices[int(len(indices) * test_split) : int(len(indices) * (test_split + val_split))]
		X_val = [days_X[i] for i in val_indices]
		y_val = [days_y[i] for i in val_indices]

		train_indices = indices[int(len(indices) * (test_split + val_split)):]
		X_train = [days_X[i] for i in train_indices]
		y_train = [days_y[i] for i in train_indices]

		return X_train, X_val, X_train_val, X_test, days_X, y_train, y_val, y_train_val, y_test, days_y


	def sampling(self, datasets, y_train_val, method, seed, n_jobs=1):
		"""unused"""
		
		if method != None:
			# Get resampler
			if method == 'adasyn':
				resampler = ADASYN(n_jobs=n_jobs, random_state=seed)
			elif method == 'randomOverSampling':
				resampler = RandomOverSampler(random_state=seed)
			elif method == 'randomUnderSampling':
				resampler = RandomUnderSampler(random_state=seed, sampling_strategy='majority')
			elif method == 'smote':
				resampler = SMOTE(n_jobs=n_jobs, random_state=seed)
			elif method == 'smoteEnn':
				resampler = SMOTEENN(n_jobs=n_jobs, random_state=seed)
			elif method == 'smoteTomek':
				resampler = SMOTETomek(n_jobs=n_jobs, random_state=seed)
			else:
				raise ValueError('Resampling method not recognised:', method)

			# resampler

		return datasets, y_train_val


	def scale_data(self, method, datasets):
		"""Scales data and checks for NaN and inf. Allowed methods: None, minmax, normalize or standard
    
		:param method: scaling method (str or None)
		:returns: scaled data
		"""

		if method != None:
			method = method.lower()
			if method == 'minmax':
				scaler = MinMaxScaler()
			elif method == 'normalize':
				scaler = Normalizer()
			elif method == 'robust':
				scaler = RobustScaler()
			elif method == 'standard':
				scaler = StandardScaler()
			else:
				raise ValueError('Unrecognised scaling method selected', method)

			X_train_val = pd.concat(datasets['X_train_val'])
			cols = list(X_train_val.columns)
			# Scales by column instead of whole dataframe to prevent scaler converting dataframe to ndarray
			X_train_val[cols] = scaler.fit_transform(X_train_val[cols])
			datasets['X_train_val'] = X_train_val

			for key, df in datasets.items():
				if key != 'X_train_val':
					# if key != 'X':
					df = pd.concat(df) # join days back into a dataframe for scaling
					df[cols] = scaler.transform(df[cols])

				# df.to_csv(key + '_scaled.csv')
				assert not np.isnan(df.values).any()
				assert np.isfinite(df.values).all()

				if key != 'X':
					# Revert to original form
					datasets[key] = self._data_by_days(df, None)
		return datasets


	def impute(self, df):
		'''Forward and back fill missing values in dataframe (ignoring the target variable)'''
		for col in df.columns:
			df[col] = df[col].fillna(method='bfill') # backward fill
			df[col] = df[col].fillna(method='ffill') # forward fill
		return df


	def drop_lag_features(self, df):
		'''Drop features representing a window'''
		for col in df.columns:
			# if not any(char.isdigit() for char in col): # TODO: better?
			if any((part.isdigit() or part.split('_')[0].isdigit()) for part in col.split('-')):
				df = df.drop(col, axis=1)
		return df


	def feature_selection(self, X, y, method='f_regression', **kwargs):
		"""Performs feature selection on a dataset
    
		Arguments:
			X {pd.Dataframe} -- feature matrix
			y {pd.Series} -- target vector
    
		Keyword Arguments:
			method {str} -- feature selection method (default: {'rf'})
    
		Raises:
			ValueError: Occurs if unexpected 'method' called
    
		Returns:
			pd.DataFrame, pd.Series, pd.DataFrame -- Returns data and computed feature weights
		"""

		method = method.lower()
		cols = list(X.columns)
		n_features = kwargs.get('n_features', 1)

		if method == 'f_regression':
			c.logger.debug('Applying f_regression feature selection...')
			selector = SelectKBest(f_regression, n_features)
			reduced_X = selector.fit_transform(X, y)
			weights = pd.DataFrame({'feature': cols, 'weight': selector.scores_})
    
		elif method == 'mutual_info_regression' or method == 'mi':
			c.logger.debug('Applying mutual_info_regression feature selection...')
			selector = SelectKBest(mutual_info_regression, n_features)
			reduced_X = selector.fit_transform(X, y)
			weights = pd.DataFrame({'feature': cols, 'weight': selector.scores_})

		# elif method == 'rf' or method == 'rrelieff':
		#   c.logger.debug('Applying RF feature selection...')
		#   selector = RReliefF(n_jobs=1)#, n_features=kwargs.get('n_features', 1))
		#   reduced_X = selector.fit_transform(X.values, y.values)
		#   weights = pd.DataFrame({'feature': cols, 'weight': selector.w_})

		# elif method == 'rf2':
		#   c.logger.debug('Applying RF2 feature selection...')
		#   selector = ReliefF(n_jobs=1)#, n_features=kwargs.get('n_features', 1))
		#   Scores = selector.fit_transform(X.values, y.values)
		#   weights = pd.DataFrame({'feature': cols, 'weight': Scores})

		# elif method == 'multisurf':
		#   c.logger.debug('Applying MultiSURF feature selection...')
		#   selector = MultiSURF(n_jobs=1)#, n_features=kwargs.get('n_features', 1))
		#   Scores = selector.fit_transform(X.values, y.values)
		#   weights = pd.DataFrame({'feature': cols, 'weight': Scores})

		elif method == None:
			weights = None
			c.logger.debug('Applying no selection...')

		else:
			raise ValueError(f'Unexpected method {method}.')

		return reduced_X, y, weights
  
  
	def drop_recent_features(self, df, target='ROI-DA', lag=24):
		"""Drop features not derived from the target if they are within a window"""
		cols = []
		for col in df.columns:
			if target in col:
				cols.append(col)
			else:
				for part in col.split('-'):
					if part.split('_')[0].isdigit() and int(part.split('_')[0]) > lag:
						cols.append(col)
		df = df[cols]
		return df


	def cap_outliers(self, df, cap=2):
		for col in df.columns:
			series = df[col]
			df[col] = series[ (series - series.mean()) <= cap * series.std() ]
		return df