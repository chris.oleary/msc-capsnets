from itertools import combinations
import math
import os
import random
import shutil
from time import perf_counter

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_selection import RFE
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf

import forecast.constants as c
from forecast.data import DataHandler
from forecast.models import ModelRunner
import forecast.util as util


class Task():
	"""
	An ad-hoc task that follows specific steps. This class attempts to add structure and repeatability to the process
	"""

	def __init__(self, **kwargs):
		"""Creates a task and sets some default parameters"""
		if len(kwargs) > 0:
			self.params = kwargs
		else:
			self.params = { 'pp_params': c.preprocessing_params }

		##### Default parameters #####
		lag = 168
		# Lag/window size
		self.params['lag'] = kwargs.get('lag', lag)
		# Input dataset path
		self.params['dataset_path'] = kwargs.get('dataset_path', os.path.join(c.DATA_DIR, f'dataset-{lag}.csv'))
		# The number of best features to use (as measured by feature importance scores)
		self.params['num_features'] = kwargs.get('num_features', 1)
		# Number of iterations to repeat operation
		self.params['num_iterations'] = kwargs.get('num_iterations', 1)
		# Feature scaling to use
		self.params['scaling'] = kwargs.get('scaling', None)
		# Default path for feature importance scores file
		self.params['scores_dir'] = kwargs.get('scores_dir', os.path.join(c.DATA_DIR, self.params['dataset_path'].split(os.sep)[-1].split('.')[0]))
		# Model parameter seearch strategy
		self.params['search'] = kwargs.get('search', None)
		# Default results file
		self.params['results_path'] = kwargs.get('results_path', os.path.join(c.RESULTS_DIR, c.RESULTS_FILE))
		# Name of target column in dataset
		self.params['target'] = kwargs.get('target', 'ROI-DA')
		# Percentage of dataset taken for test set
		self.params['test_split'] = kwargs.get('test_split', c.test_split)
		# Run Keras models
		self.params['use_keras'] = kwargs.get('use_keras', True)
		# Run K-SVM-SVR model
		self.params['use_k_svm_svr'] = kwargs.get('use_k_svm_svr', True)
		# Run scikit-learn models
		self.params['use_sklearn'] = kwargs.get('use_sklearn', True)
		# Percentage of dataset taken for validation set
		self.params['val_split'] = kwargs.get('val_split', c.val_split)
		# If false, keras models will use validation set in dily batches, but will instead validate as normal (for callbacks)
		self.params['validate_by_day'] = kwargs.get('validate_by_day', c.validate_by_day)


	def run(self, **kwargs):
		"""Run an operation"""

		raise NotImplementedError('Task run function not yet implemented')


	def check_kwargs(self, expected_names, kwargs):
		"""Asserts that all expected keys are in keyword arguments"""

		for name in expected_names:
			assert name in kwargs


class TaskRunner():
	"""Run tasks based on configuration"""

	def __init__(self, cli_args, **kwargs):
		self.TASKS = {
			1: (JoinIsemCsvFiles, 'Join the 5 provided I-SEM CSV data files into a combined CSV with processing'),
			2: (CalculatePerformance, 'Calculate the performance from a CSV file containing values and predictions'),
			3: (FeatureSelection, 'Feature selection with plots'),
			4: (RunModels, 'Run all implemented forecasting models'),
			5: (BenchmarkImports, 'Measure how long it takes to import libraries/modules'),
			6: (GenerateSummary, 'Generate summary files'),
			7: (ChannelAndFeatureCorrelation, 'Determine the relationships between dataset variables'),
			8: (ChannelStatistics, 'Records some statistics for each channel'),
			9: (ParameterPerfomanceCorrelation, 'Determine relationship between model parameters and performance'),
			10: (CleanResultsFiles, 'Clean result CSV files'),
		}

		c.NUM_JOBS = cli_args.num_cores
		c.TASK_NUM = cli_args.task_num
		task_num = cli_args.task_num
		kwargs = { **kwargs, **cli_args.__dict__ }

		if task_num not in self.TASKS:        
			c.logger.error('Unexpected task number; ' + str(task_num) + '. Available options are:')
			for k, v in self.TASKS.items():
				c.logger.error(str(k) + '-> ' + v[1])
			raise ValueError('Unexpected task number', task_num)
		else:
			c.logger.info('Running task ' + str(task_num) + ': ' + self.TASKS[task_num][1])
			task = self.TASKS[task_num][0](**kwargs)
			task.run(**kwargs)


class JoinIsemCsvFiles(Task):
	"""Given the provided 5 CSV files, create a dataset for modelling"""

	def __init__(self, **kwargs):
		super(JoinIsemCsvFiles, self).__init__(**kwargs)


	def run(self, **kwargs):
		"""
		Join 5 expected csv files into one csv file, dropping some metadata in 
		the process. This files should follow an expected format as provided by CIT.
		"""

		self.lag = self.params['lag']

		self.date_col = 'start_time'
		self.market_col = 'market_name'
		self.currency_col = 'currency'
		PLACEHOLDER = -100000.0 # Massively negative will allow for easy identification during imputation

		cols = [ 'start_time', 'load_forecast_ni', 'load_forecast_roi', 'aggregated_forecast' ]
		imbalance_cols = [
			'start_time', 'total_pn', 'net_interconnector_schedule', 'tso_demand_forecast', 
			'tso_renewable_forecast', 'calculated_imbalance'
		]
		# applicable_date renamed to start_time
		prediction_cols = [ 'start_time', 'val', 'market_name', 'currency' ]
		price_cols = [ 'start_time', 'market_name', 'val', 'currency' ]

		files = {
			'prices.csv': ([1, 3, 4, 6], price_cols),
			'dailyload.csv': ([5, 7, 8, 9], cols), 
			'four_day_aggregate_wind.csv': ([5, 7, 8, 9], cols), 
			'imbalance.csv': ([4, 6, 7, 8, 9, 10], imbalance_cols),
			# 'predicted_prices.csv': (range(1, 5), prediction_cols), 
		}
		metadata = list()
		metadata.append('Using a lag of {} days\n'.format(self.lag))
		metadata.append('Files used: {}\n'.format(list(files.keys())))

		merged_df = None
		for k, v in files.items():
			c.logger.debug('Reading ' + str(k))
			df = pd.read_csv('data/' + k, usecols=v[0], header=None)
			df.columns = v[1]
			df = df.replace('\\N', PLACEHOLDER)

			# check for gaps/inconsistencies
			# df = util.check_intervals(df)

			if k == 'prices.csv':
				start_time = df[self.date_col].min()
				end_time = df[self.date_col].max()
				metadata.append('DAM prices range from: {} to {}\n'.format(start_time, end_time))

				index = pd.date_range(start_time, end_time, freq='H')
				merged_df = pd.DataFrame()
				merged_df['index'] = index
				merged_df = merged_df.set_index('index')

			df[self.date_col] = pd.to_datetime(df[self.date_col], errors='coerce')
			df = df[ # i-sem uses hourly prices
				(df[self.date_col].dt.minute != 15) & 
				(df[self.date_col].dt.minute != 30) & 
				(df[self.date_col].dt.minute != 45)
			]
			c.logger.debug('Date range for ' + str(k) + ' is ' + str(df[self.date_col].min()) + ' to ' + \
				str(df[self.date_col].max()))
			df = df.set_index(self.date_col)
			df.sort_index(inplace=True)

			if self.market_col in df.columns:
				# df = self.divide_by_market(df)
				df = df[df[self.market_col] == 'ROI-DA']
				df = df[df[self.currency_col] == 'EUR']
				df['ROI-DA'] = df['val']
				df = df.drop('val', 1)
				df = df.drop(self.market_col, 1)
				df = df.drop(self.currency_col, 1)

			# Format columns and their values
			for col in df.columns:
				df[col] = pd.to_numeric(df[col])
				df[col] = df[col].astype('float64')

			df = df.loc[~df.index.duplicated(keep='first')] # remove rows with duplicate indices
			df = self.apply_lag(df)
			merged_df = merged_df.join(df, how='outer', rsuffix='_'+str(k.split('.')[0]))

		# for col in merged_df.columns:
		#   if col == 'ROI-DA':
		#     merged_df[col] = merged_df[col].fillna(method='ffill') # forward fill missing values

		merged_df = merged_df.truncate(after=end_time, before=start_time) # truncate dataframe to start and end of price data
		# merged_df[] = merged_df[col].fillna(PLACEHOLDER) # replace with placeholder
		# merged_df.replace('NaN', PLACEHOLDER)
		merged_df = self.encode_hour(merged_df)
		merged_df.sort_index(inplace=True)

		c.logger.debug('Final shape: ' + str(merged_df.shape))
		metadata.append('Final shape: {}\n'.format(merged_df.shape))
		util.check_intervals(merged_df, col='ROI-DA')

		# Save data as CSV and record meta data in a text file
		merged_df.to_csv(os.path.join(c.DATA_DIR, 'dataset-'+str(self.lag)+'.csv'), index=True)
		with open(os.path.join(c.DATA_DIR, 'metadata-{}.txt'.format(self.lag)), 'w+') as f:
			f.writelines(metadata)
		return merged_df


	def divide_by_market(self, df):
		"""DEPRECATED. Divide a dataframe by market type """
		market_names = df[self.market_col].unique()

		df_new = df[df[self.market_col] == market_names[0]]
		for market in market_names:#[1:]:
			if 'ROI-DA' == market:
				df_market = df[df[self.market_col] == market] # get rows for a market
				df_market = df_market[['val']]
				df_market = df_market.loc[~df_market.index.duplicated(keep='first')] # remove rows with duplicate indices
				df_new = df_new.join(df_market, how='outer', lsuffix='_'+str(market))

		df_new = df_new.drop('val', 1)
		df_new = df_new.drop(self.market_col, 1)
		return df_new


	def apply_lag(self, df):
		"""Apply a time series lag to create more features"""
		if self.lag > 0:
			c.logger.debug('Applying lag of ' + str(self.lag) + ' to ' + str(df.columns.tolist()))
			for col in df.columns:
				for i in range(1, self.lag):
					df[col+'-'+str(i)] = df[col].shift(i)
		return df


	def encode_hour(self, df):
		"""Create sin/cos time features from the hour of the index"""
		df['hour_sin'] = np.sin(2 * np.pi * df.index.to_series().apply(lambda x: int(x.hour))/24.0)
		df['hour_cos'] = np.cos(2 * np.pi * df.index.to_series().apply(lambda x: int(x.hour))/24.0)
		return df


class CalculatePerformance(Task):
	"""Given predictions calculate error metrics"""

	def run(self, **kwargs):
		# Read data
		df_data = pd.read_csv(os.path.join(c.DATA_DIR, c.DATA_FILE), index_col=0)
		df_predictions = pd.read_csv(os.path.join(c.DATA_DIR, 'predicted_prices.csv'), header=None)
		df_predictions.columns = [ 'id', 'applicable_date', 'val', 'market_name', 'currency' ]

		# Select relevant data from prices
		df = df_data[['ROI-DA']]
		num_rows = df.shape[0]
		df = df.dropna()
		dropped_rows = num_rows - df.shape[0]
		c.logger.info('Dropping ' + str(dropped_rows) + ' rows from true prices')

		# Select relevant data from predictions
		df_predictions = df_predictions[df_predictions['market_name'] == 'ROI-DA']
		df_predictions = df_predictions[df_predictions['currency'] == 'EUR']
		df_predictions = df_predictions.set_index('applicable_date')
		df_predictions = df_predictions[['val']]

		# Merge information and drop rows with no predictions
		df = df.join(df_predictions, how='left', rsuffix='_test')
		df = df.dropna()

		scores = util.get_scores(df['ROI-DA'], df['val'])
		c.logger.info('Model Scores:')
		for score in scores:
			c.logger.info(str(score) + ': ' + str(scores[score]))
		return scores


class FeatureSelection(Task):
	"""Create plots for a time series dataset: ACF, PACF"""

	def run(self, **kwargs):
		ACF_PLOTS = kwargs.get('acf_plots', True)
		F_REGRESSION = kwargs.get('f_regression', True)
		IMPORTANCE_SCORES = kwargs.get('importance_scores', False)
		MI = kwargs.get('mi', True)
		NUM_ITERATIONS = kwargs.get('num_iterations', 1)
		PACF_PLOTS = kwargs.get('pacf_plots', True)
		RF_RANK = kwargs.get('rf_rank', True)
		MULTISURF = kwargs.get('multisurf', True)
		RFE_SCORES = kwargs.get('rfe_scores', False)
		TARGET = kwargs.get('target', 'ROI-DA')

		X, y, df = self._read_data(self.params['dataset_path'], TARGET)

		output_dir = os.path.join(c.PLOT_DIR, self.params['dataset_path'].split(os.sep)[1].split('.')[0])
		util.create_dir(os.path.join(output_dir, 'ACF'))
		util.create_dir(os.path.join(output_dir, 'PACF'))

		# for i in range(NUM_ITERATIONS): # TODO
		X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.8, random_state=c.SEED)#+i)
		data_handler = DataHandler()

		if ACF_PLOTS or PACF_PLOTS:
			self._generate_plots(df, output_dir, ACF_PLOTS, PACF_PLOTS)

		model_params = { 
			'n_estimators': kwargs.get('n_estimators', 100), 
			'random_state': kwargs.get('random_state', 0), 
			'n_jobs': kwargs.get('n_jobs', 1), 
			'verbose': c.SKLEARN_LEVEL
		}

		# The following are roughly ordered by estimated run time; quickest operations come first

		if F_REGRESSION:
			X, y, weights = data_handler.feature_selection(X_train, y_train, method='f_regression', n_features=1)
			weights.to_csv(os.path.join(output_dir, f'f_regression.csv'), index=False)

		if MI:
			X, y, weights = data_handler.feature_selection(X_train, y_train, method='mi', n_features=1)
			weights.to_csv(os.path.join(output_dir, f'mi.csv'), index=False)

		if IMPORTANCE_SCORES:
			cols, importance_scores = self._importance_scores(X_train, y_train, model_params=model_params)
			df = pd.DataFrame({'feature': cols, 'importance': importance_scores})
			df.to_csv(os.path.join(output_dir, 'importance.csv'), index=False)

		if RF_RANK:
			# for i in range(30):
			X, y, weights = data_handler.feature_selection(X_train, y_train, method='rf2', sigma=0.1)#, n_features=5)
			weights.to_csv(os.path.join(output_dir, f'rf2_rank.csv'), index=False)
			print(os.path.join(output_dir, f'rf2_rank.csv'))

		if MULTISURF:
			X, y, weights = data_handler.feature_selection(X_train, y_train, method='multisurf', sigma=0.1)#, n_features=5)
			weights.to_csv(os.path.join(output_dir, f'multisurf_rank.csv'), index=False)
			print(os.path.join(output_dir, f'multisurf_rank.csv'))

		if RF_RANK:
			# for i in range(30):
			X, y, weights = data_handler.feature_selection(X_train, y_train, method='rf', sigma=0.1)#, n_features=5)
			weights.to_csv(os.path.join(output_dir, f'rf_rank.csv'), index=False)
			print(os.path.join(output_dir, f'rf_rank.csv'))

		if RFE_SCORES:
			cols, rfe_scores = self._rfe_scores(X_train, y_train, model_params=model_params)
			df = pd.DataFrame({'feature': cols, 'rank': rfe_scores})
			df.to_csv(os.path.join(output_dir, 'rfe.csv'), index=False)


	def _read_data(self, data_path, target_col):
		"""Read dataset and split into features matrix and target vector. Missing values are imputed
        
		:param data_path: path to dataset
		:param target_col: the name of the target column
		:returns: feature matrix, target vector and dataframe
		"""

		c.logger.debug('Reading data...')
		start = perf_counter()
		df = pd.read_csv(data_path, index_col=0)
		# Impute values
		df = DataHandler().impute(df)
		# Get features (X) and target variable (y)
		y = df[target_col]
		X = df.copy(deep=True)
		X = X.drop(target_col, axis=1)
		c.logger.debug('Took ' + str(round(perf_counter() - start, 2)) + ' seconds')
		return X, y, df


	def _generate_plots(self, df, output_dir, acf_plots=True, pacf_plots=True):
		"""Generate ACF and PACF plots for time series features where.
        
		:param df: dataframe of time series data
		:param acf_plots: if True, ACF plots are generated
		:param pacf_plots: if True, PACF plots are generated
		:returns: None
		"""
		if not acf_plots and not pacf_plots:
			c.logging.warning('Called _generate_plots(), but ACF and PACF are False')

		for col in df.columns:
			if not any(char.isdigit() for char in col): # doesn't contain digits (which would be lag cols)
				with plt.rc_context():
					plt.rc("figure", figsize=(20, 4))
					series = df[col].dropna()

					if acf_plots:
						fig = plot_acf(series, unbiased=True, missing='drop', lags=200, title=col+'-ACF.png') # plot ACF
						fig.savefig(os.path.join(output_dir, 'ACF', col+'-ACF.png'))
						fig.clear()
						plt.close(fig)

					if pacf_plots:
						fig = plot_pacf(series, lags=200, title=col+'-PACF.png') # plot PACF
						fig.savefig(os.path.join(output_dir, 'PACF', col+'-PACF.png'))
						fig.clear()
						plt.close(fig)

					plt.clf()


	def _importance_scores(self, X, y, model_params):
		"""Calculate feature importance scores using Random Forest.
        
		:param X: feature matrix
		:param y: target vector
		:param model_params: model parameters
		"""

		c.logger.debug('Fitting model...')
		start = perf_counter()
		model = RandomForestRegressor()
		model.set_params(**model_params)
		model.fit(X, y)
		c.logger.debug('Took ' + str(round(perf_counter() - start, 2)) + ' seconds')
		return list(X.columns), model.feature_importances_


	def _rfe_scores(self, X, y, model_params, n_features_to_select=1):
		"""Use Recursive Feature Elimination (RFE) to rank features

		n_features_to_select is a parameter of RFE. 1 = rank all features.
        
		:param X: the feature matrix
		:param y: the target vector
		:param model_params: model parameters
		:param n_features_to_select: number of features to select for ranking
		"""

		c.logger.debug('Fitting model for RFE...')
		start = perf_counter()
		model = LinearRegression() # RandomForestRegressor()
		# model.set_params(**model_params)
		rfe = RFE(model, n_features_to_select)
		fit = rfe.fit(X, y)
		c.logger.debug('Took' + str(round(perf_counter() - start, 2)) + 'seconds')
		return list(X.columns), fit.ranking_


class RunModels(Task):
	"""Run available models on I-SEM data"""

	def __init__(self, **kwargs):
		super(RunModels, self).__init__(**kwargs)


	def run(self, **kwargs):

		# Read data and impute for dependent variables
		data_handler = DataHandler()
		c.logger.info('Using file: ' + self.params['dataset_path'])
		df = data_handler.read_data(self.params['dataset_path'])

		# Target variable
		y = df[self.params['target']]
		df = df.drop(self.params['target'], axis=1)

		for i in range(self.params['num_iterations']):
			c.SEED = random.randint(0, 10000000)
			c.logger.info('Iteration ' + str(i + 1) + ' of ' + str(self.params['num_iterations']))

			for metric, limits in c.feature_selection.items():
				for limit in limits:
					self.params['num_best_features'] = limit
					self.params['feature_selection'] = metric
					# Get feature importance scores
					scores_path = os.path.join(c.PLOT_DIR, f'dataset-{self.params["lag"]}', f'{metric}.csv')
					df_fs = pd.read_csv(scores_path)
					df_fs = df_fs.sort_values('weight', ascending=False)

					features = df_fs.head(limit)['feature'].values.tolist()
					X = df[[f for f in features if f in df.columns]] # all but i features if present
					c.logger.info(f'Using {self.params["num_features"]} feature(s)')

					data = data_handler.split_data(X, y, **self.params)
					ModelRunner().train_ml_models(data, **self.params)


class BenchmarkImports(Task):
	"""Used when measuring worst case program execution time during imports"""

	def run(self, **kwargs):
		import forecast.constants
		import forecast.capsnet
		import forecast.data
		import forecast.keras_models
		import forecast.k_svm_svr
		import forecast.models
		import forecast.tasks
		import forecast.util
        

class GenerateSummary(Task):
	"""Generate summary files"""

	def __init__(self, **kwargs):
		super(GenerateSummary, self).__init__(**kwargs)


	def run(self, **kwargs):
		self.results_path = kwargs.get('results_path', self.params['results_path'])

		path_parts = self.results_path.split('.')
		df_full = pd.read_csv(self.results_path)

		# 1. Record failed training attempts for each model
		df = df_full[['model', 'num_fails', 'test_MAE']]
		df['num_fails'] = df['num_fails'].astype('float64')
		df = df.sort_values('model', ascending=True)
		info = []

		# Fails per model
		for model in df_full['model'].unique():
			model_scores = df[df['model'] == model]

			num_fail_train = model_scores[model_scores["test_MAE"] == "FAIL"].shape[0]
			num_fail_pred = model_scores[model_scores["num_fails"] > 0].shape[0]
			num_inf = model_scores[model_scores["test_MAE"] == "inf"].shape[0]
			num_minus_inf = model_scores[model_scores["test_MAE"] == "-inf"].shape[0]
            
			if num_fail_pred > 0 or num_fail_train > 0 or num_inf > 0 or num_minus_inf > 0:
				info.append(f'Number of fails for {model} -> Failed training: {num_fail_train}, failed predictions: {num_fail_pred}, inf: {num_inf}, -inf: {num_minus_inf}')

		# Total fails
		info.append(f'Total number of failed models: {df[df["test_MAE"] == "FAIL"].shape[0]}')
		# Record fails in text file
		with open(path_parts[0] + '-fails.txt', 'w') as f:
			for line in info:
				f.write(line + '\n')

		# 2. Remove fails and infinite values
		df_full['test_MAE'] = df_full['test_MAE'].replace([np.inf, -np.inf, np.nan, 'FAIL', 'inf', '-inf', '#NAME?'], 1000000.0)
		df_full['val_MAE'] = df_full['val_MAE'].replace([np.inf, -np.inf, np.nan, 'FAIL', 'inf', '-inf', '#NAME?'], 1000000.0)
		df_full['test_MAE'] = df_full['test_MAE'].astype('float64')
		df_full['val_MAE'] = df_full['val_MAE'].astype('float64')

		# 3. Boxplot of best score for each model for each seed
		# Stats for test MAEs for best validation MAE for each seed
		indices = []
		best_MAE = {}
		best_MAE2 = {}
		mae_scores = {}
		for model in df_full['model'].unique():
			df_model = df_full[df_full['model'] == model]
			mae_scores[model] = df_model['test_MAE']
			best_MAE[model] = df_model['test_MAE'].min()
			best_MAE2[model] = df_model['test_MAE2'].min()
            
			# get indices of best models for each seed
			for seed in df_model['seed'].unique():
				df_seed = df_model[df_model['seed'] == seed]

				if df_seed.shape[0] > 1:
					# df_seed = df_seed[['model', 'val_MAE2', 'test_MAE']]
					indices.append(df_seed['val_MAE'].idxmin())

		with open(path_parts[0] + '-best_scores.txt', 'w') as f:
			for k, v in best_MAE.items():
				f.write(f'{k}-> MAE: {best_MAE[k]} MAE2: {best_MAE2[k]} \n')


		# Calculate statistical significances
		models = list(mae_scores.keys())
		results = []
		for combo in combinations(models, 2):
			a = mae_scores[combo[0]]
			b = mae_scores[combo[1]]
			if a.shape[0] > 1 and b.shape[0] > 1:
				# Levene's test
				l_stat, p_val_l = stats.levene(a, b)
				lev_result = 'equal' if p_val_l > 0.05 else 'unequal' 

				# Welch's t-test (unequal variances t-test)
				t_stat_w, p_val_w = stats.ttest_ind(a, b, nan_policy='raise', equal_var=False)
				wel_result = 'significant' if p_val_w <= 0.05 else 'insignificant' 

				# Students's t-test (equal variances t-test)
				t_stat_s, p_val_s = stats.ttest_ind(a, b, nan_policy='raise', equal_var=True)
				stu_result = 'significant' if p_val_s <= 0.05 else 'insignificant' 

				results.append(f'{combo}, L: {p_val_l} ({lev_result}), W: {p_val_w} ({wel_result}), S: {p_val_s}  ({stu_result})\n')
		
		for model in models:
			# normal = stats.normaltest(mae_scores[model])
			if mae_scores[model].shape[0] > 1:
				stat, p_val = stats.normaltest(mae_scores[model])
				normal = 'normal' if p_val <= 0.05 else 'not normal'
				results.append(f'{model}: {p_val} ({normal})\n')

		# Save statistical significances to file
		with open(path_parts[0] + '-statistical-significance.txt', 'w') as f:
			f.writelines(results)

		# Get rows of best models
		best_scores = df_full.loc[indices]
		# best_scores = best_scores[['model', 'scaling', 'feature_selection', 'test_MAE', 'test_MAE2', 'time']]

		# Group results into categories
		results = []
		for model in best_scores['model'].unique(): 
			df_model = best_scores[best_scores['model'] == model]

			for feature_selection in df_model['feature_selection'].unique():
				df_fs = df_model[df_model['feature_selection'] == feature_selection]

				for num_features in df_fs['num_best_features'].unique():
					df_nf = df_fs[df_fs['num_best_features'] == num_features]

					for num_iterations in df_fs['num_iterations'].unique():
						df_ni = df_fs[df_fs['num_iterations'] == num_iterations]
					
						for scaling in df_ni['scaling'].unique():
							df = df_ni[df_ni['scaling'] == scaling]

							results.append({ 
								'Model': model, 
								'Feature Selection': feature_selection, 
								'Num. Features': num_features, 
								'Num. Iterations': num_iterations, 
								'Scaling': scaling, 
								'MAE': df['test_MAE'].mean(),
								'median MAE': df['test_MAE'].median(),
								# 'std. MAE': df['test_MAE'].std(),
								'MAE2': df['test_MAE2'].mean(),
								'median MAE2': df['test_MAE2'].median(),
								# 'std. MAE2': df['test_MAE2'].std(),
								# 'MAPE': df['test_MAPE'].mean(),
								# 'MSE': df['test_MSE'].mean(),
								# 'Spearman Rank correlation': df['test_SRC correlation'].mean(),
								'Mean Time': df['time'].mean(),
								'Median Time': df['time'].median(),
							})

		# Remove old summary file
		results_path = path_parts[0] + '-summary.csv'
		if os.path.exists(results_path):
			os.remove(results_path)

		# Save new summary file
		for result in results:
			util.results_to_file(result, results_path)


class ChannelAndFeatureCorrelation(Task):
	"""Investigates the relationship between variables"""

	def __init__(self, **kwargs):
		super(ChannelAndFeatureCorrelation, self).__init__(**kwargs)

	def run(self):

		data_handler = DataHandler()
		df = data_handler.read_data(self.params['dataset_path'])

		output_dir = os.path.join(c.PLOT_DIR, self.params['dataset_path'].split(os.sep)[1].split('.')[0])
		util.create_dir(os.path.join(output_dir))

		# 1. Correlation between all features and target
		os.remove(os.path.join(output_dir, 'target_feature_correlation.csv'))
		target = df[self.params['target']]
		for col in df.columns:
			if col != 'target':
				variable = df[col]
				pearson_corr, _ = stats.pearsonr(variable, target) # Pearson's correlation (strength of linear relationship)
				spearman_corr = stats.spearmanr(variable, target).correlation # Spearman's correlation (strength of non-linear relationship)
				result = { 'variable': col, 'pearson_corr': pearson_corr, 'spearman_corr': spearman_corr }
				util.results_to_file(result, os.path.join(output_dir, 'target_feature_correlation.csv'))

		# 2. Correlation between all variables
		df = data_handler.drop_lag_features(df)
		corr_df = df.corr()
		fig = plt.figure(figsize=(30, 30))

		plt.matshow(corr_df, fignum=fig.number)
		plt.xticks(range(df.shape[1]), df.columns, fontsize=12, rotation=90)
		plt.yticks(range(df.shape[1]), df.columns, fontsize=12)
		
		cb = plt.colorbar()
		cb.ax.tick_params(labelsize=14)

		plt.savefig(os.path.join(output_dir, 'variable_correlation.png'))
		plt.show()
		plt.clf()

		corr_df.to_csv(os.path.join(output_dir, 'variable_correlation.csv'), index=True)


class ChannelStatistics(Task):
	"""Records some statistics for each channel"""

	def __init__(self, **kwargs):
		super(ChannelStatistics, self).__init__(**kwargs)

	def run(self, **kwargs):
		data_handler = DataHandler()
		df = data_handler.read_data(self.params['dataset_path'])
		df = data_handler.drop_lag_features(df)
		
		info = []
		for col in df.columns:
			series = df[col]
			info.append(f'\nColumn: {col}')
			
			if col == self.params['target']:
				target_col = df[[col]]
				target_col.index = pd.to_datetime(target_col.index, errors='coerce')

				periods = { 
					'Y': ('year', target_col.index.year), 
					'M': ('month', target_col.index.month), 
					'H': ('hour', target_col.index.hour), 
					}

				for key, period in periods.items():
					self._plot(target_col, col, period)

					grouped = target_col.groupby(pd.Grouper(freq=key))
					info.append(f'\nMedian ({period[0]}): {grouped.median()}')
					info.append(f'\nStd ({period[0]}): {grouped.std()}')
					info.append(f'\nMax ({period[0]}): {grouped.max()}')
					info.append(f'\nMin ({period[0]}): {grouped.max()}')

			info.append(f'count: {series.shape[0]}')
			info.append(f'missing: {series.isna().sum()}')
			info.append(f'mean: {series.mean()}')
			info.append(f'median: {series.median()}')
			info.append(f'std: {series.std()}')

			for i in range(1, 5):
				outliers = series[ abs(series - series.mean()) > i * series.std() ]
				series = series[ abs(series - series.mean()) <= i * series.std() ]
				info.append(f'{outliers.shape[0]} values more than {i} standard deviations from mean')
				info.append(f'After removal -> mean: {series.mean()}, median: {series.median()}, std: {series.std()}')

		with open(os.path.join(c.DATA_DIR, 'stats.txt'), 'w') as f:
			for line in info:
				f.write(line + '\n')


	def _plot(self, target_col, col, period):
		target_col['Price (€)'] = target_col[col]

		# Box plot
		plt.figure(figsize=(10, 5))
		plt.xlabel(period[0])
		plt.ylabel('Price (€)')
		axes = sns.boxplot(x=period[1], y=target_col['Price (€)'])
		fig = axes.get_figure()
		fig.savefig(os.path.join(c.DATA_DIR, period[0] + 'box'))

		# Delete plot
		fig.clf()
		plt.close(fig)
		del fig
		del axes

		# Bar plot
		plt.figure(figsize=(10, 5))
		plt.xlabel(period[0])
		plt.ylabel('Price (€)')
		axes = sns.barplot(x=period[1], y=target_col['Price (€)'])
		fig = axes.get_figure()
		fig.savefig(os.path.join(c.DATA_DIR, period[0] + 'bar'))

		# Delete plot
		fig.clf()
		plt.close(fig)
		del fig
		del axes


class ParameterPerfomanceCorrelation(Task):

	def __init__(self, **kwargs):
		super(ParameterPerfomanceCorrelation, self).__init__(**kwargs)


	def run(self, **kwargs):
		files = [
			f for f in os.listdir(c.RESULTS_DIR) 
			if (f.endswith('csv') and not f.startswith('results') and not f.startswith('predictions'))
			]

		for file_name in files:
			file_path = os.path.join(c.RESULTS_DIR, file_name)
			df = pd.read_csv(file_path)
			print(file_path)

			if df.shape[0] > 1:
				df['test_MAE'] = df['test_MAE'].replace([np.inf, -np.inf, np.nan, 'FAIL', 'inf', '-inf'], 1000000.0)
				df['val_MAE'] = df['val_MAE'].replace([np.inf, -np.inf, np.nan, 'FAIL', 'inf', '-inf'], 1000000.0)
				df['MAE'] = df['test_MAE']
				df['MAE2'] = df['test_MAE2']
                
				cols = []
				unused_cols = [
					'MAE', 'model', 'verbose', 'num_fails', 
					'num_best_features', 'seed', 'time', 
					'random_state', 'input_file'
					]
				for col in df.columns:
					if 'val_' not in col and 'test_' not in col and col not in unused_cols:
						cols.append(col)

				result_dir = file_path.split('.')[0]
				util.create_dir(result_dir)

				scores = []
				for limit in [ 'none', 'log', 1000, 100, 50 ]:
					df['MAE'] = df['MAE'].astype('float64').clip(0, 1000000)
					target_col = df['MAE']

					for col in cols:
						var_col = df[col]
						
						if var_col.unique().shape[0] > 1:
							# Deal with NaNs or infs
							if isinstance(var_col[0], np.float64) or isinstance(var_col[0], np.int64):
								pearson_corr, _ = stats.pearsonr(df['MAE'], var_col)
								scores.append(f'{col} correlation: {pearson_corr}')
								if isinstance(var_col[0], np.int64):
									self._plot(df[[col, 'MAE']], result_dir, 'MAE', col, limit, plot_type='box')
									# self._plot(df[[col, 'MAE2']], result_dir, 'MAE2', col, limit, plot_type='box')
								else:
									self._plot(df[[col, 'MAE']], result_dir, 'MAE', col, limit, plot_type='scatter')
									# self._plot(df[[col, 'MAE2']], result_dir, 'MAE2', col, limit, plot_type='scatter')

							else:
								for option in var_col.unique():
									indices = var_col[var_col == option].index
									scores.append(f'{col}-{option} median: {target_col[indices].median()}')
									
								self._plot(df[[col, 'MAE']], result_dir, 'MAE', col, limit, plot_type='box')
								# self._plot(df[[col, 'MAE2']], result_dir, 'MAE2', col, limit, plot_type='box')
								

					with open(os.path.join(result_dir, f'scores-limit-{limit}.txt'), 'w+') as f:
						for line in scores:
							f.write(line + '\n')
			else:
				print(f'Multiple results required for analysis. Got {df.shape[0]}')


	def _plot(self, df, result_dir, metric, col, limit, plot_type='scatter'):
		# Size and labels
		plt.figure(figsize=(20, 10))
		plt.xlabel(col)
		plt.ylabel(metric)

		# Create plot
		if plot_type == 'scatter':
			axes = df.plot.scatter(x=col, y=metric, c='DarkBlue')
		else:
			axes = df.boxplot(by=col, column=[metric])
        
		# Apply limits, log scaling and save
		if not isinstance(limit, str):
			axes.set_ylim([0, limit])
		elif limit == 'log':
			plt.yscale('log')
		fig = axes.get_figure()
		fig.savefig(os.path.join(result_dir, col + f'-{limit}'))

		# Delete plot
		fig.clf()
		plt.close(fig)
		del fig
		del axes


class CleanResultsFiles(Task):
	"""Clean results files"""
	
	def __init__(self, **kwargs):
		super(CleanResultsFiles, self).__init__(**kwargs)

	
	def run(self, **kwargs):
		# Get CSV results file names
		files = [
			f for f in os.listdir(c.RESULTS_DIR) 
			if (f.endswith('csv') and not f.startswith('predictions'))
			]
		
		model_names = [ *list(c.all_sklearn_models.keys()), *list(c.stats_models.keys()) ]

		for file_name in files:
			print(file_name)
			file_path = os.path.join(c.RESULTS_DIR, file_name)
			headers = []
			lines = []

			num_lines = 0
			with open(file_path, 'r') as orig_file: # original file
				for line in orig_file:
					num_lines += 1

					# if line not empty
					if line.strip():
						# remove square brackets
						line = line.replace('[', '').replace(']', '')

						num_values = len(line.replace('\n', '').split(',')) # count values in line

						if len(headers) == 0: # Get CSV headers from first non-empty line
							headers = line.replace('\n', '').split(',')
							lines.append(line)
							
						elif num_values > len(headers): # missing line breaks
							for model in model_names:
								line = line.replace(model, f'\n{model}')
							line_parts = line.split('\n')
							lines += [f'{l}\n' for l in line_parts if len(l) > 0]

						elif num_values == len(headers): # normal line
							lines.append(line)

			# Add newline
			if not lines[-1].strip():
				lines.append('\n')
				
			# Write lines to temporary file
			with open(file_path + '.ignore', 'w') as tmp_file: # temporary file
				tmp_file.writelines(lines)

			# Check to see if file can be read into a dataframe
			df = pd.read_csv(file_path + '.ignore')

			# Replace original file with temporary file
			if os.path.exists(file_path):
				os.remove(file_path)
				os.rename(file_path + '.ignore', file_path)

			# Check again
			df = pd.read_csv(file_path)

