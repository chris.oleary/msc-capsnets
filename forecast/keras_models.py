"""
A place to to default model architectures for deep learning models. Seperate 
from constants.py to prevent importing keras modules unnecessarily. 
"""

import warnings, os, sys
if not sys.warnoptions:
  warnings.simplefilter("ignore")
  os.environ["PYTHONWARNINGS"] = "ignore" # Also affect subprocesses

import numpy as np
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau, TensorBoard, TerminateOnNaN, \
  LambdaCallback
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import BatchNormalization, Bidirectional, Dense, Dropout, Conv1D, Conv2D, \
    Flatten, GlobalMaxPooling2D, GRU, Input, LSTM, LeakyReLU, MaxPooling1D

import forecast.capsnet as capsnet
import forecast.constants as c

# Keras architectures. Formats:
# 'dense-32': a densely connected layer with 32 neurons
# 'dropout-0.5': a dropout layer (rate: 0.5)
# 'caps-1-16-3': a capsule layer with output shape 1, 16 capsules and 3 routing-by-agreement iterations
# 'conv1d-64-2': a 1D convolutional layer with 64 filters and kernel size 2
# 'gru-16-0-0': a GRU unit with 16 neurons in dense layers, dropout 0, recurrent dropout 0
# 'lstm-16-0-0': a LSTM unit with 16 neurons in dense layers, dropout 0, recurrent dropout 0
# 'maxpool-2': a max pooling with kernel size 2
# 'primary-32-8-2': a primary capsule layer containing 32 capsules of 8 convolutional filters with kernel size 2
architectures = {
    # CapsNet
    # # # 'capsnet-1': [ 'conv1d-32-2', 'conv1d-32-2', 'primary-128-8-2', 'caps-8-16-3', 'length' ], # Y. Kim
    # 'capsnet-2': [ 'conv1d-64-2', 'conv1d-32-2', 'primary-128-8-2', 'caps-8-16-3', 'length' ],
    # 'capsnet-3': [ 'conv1d-32-2', 'primary-32-8-2', 'caps-8-16-3', 'length', 'dense-8' ],
    # 'capsnet-4': [ 'conv1d-64-2', 'primary-32-8-2', 'caps-8-16-3', 'length' ],
    # 'capsnet-5': [ 'conv1d-64-2', 'conv1d-32-2', 'primary-32-8-2', 'caps-8-16-3', 'length' ], 
    # CNN
    # 'cnn-1d-1': [ 'conv1d-64-2', 'maxpool-2', 'flatten', 'dense-64' ],
    # 'cnn-1d-2': [ 'conv1d-32-2', 'maxpool-2', 'flatten', 'dense-32'  ],
    # 'cnn-1d-3': [ 'conv1d-64-2', 'conv1d-32-2', 'maxpool-2', 'flatten', 'dense-32' ],
    # 'cnn-1d-4': [ 'conv1d-64-2', 'conv1d-32-2', 'maxpool-2', 'flatten', 'dense-64', 'dense-32' ],
    # 'cnn-1d-5': [ 'conv1d-128-2', 'conv1d-64-2', 'conv1d-32-2', 'maxpool-2', 'flatten', 'dense-64', 'dense-32' ],
    # 'cnn-1d-6': [ 'conv1d-32-2', 'maxpool-2', 'flatten' ],
    # 'cnn-1d-7': [ 'conv1d-64-2', 'maxpool-2', 'flatten', ],
    # 'cnn-1d-8': [ 'conv1d-32-2', 'conv1d-32-2', 'maxpool-2', 'flatten', 'dense-16' ],
    # 'cnn-1d-9': [ 'conv1d-64-2', 'conv1d-32-2', 'maxpool-2', 'flatten', 'dense-16' ],
    # FFNN
    'dense-0': [ ],
    # 'dense-1': [ 'dense-16' ],
    # 'dense-2': [ 'dense-32' ],
    # 'dense-3': [ 'dense-32', 'dense-16' ],
    # 'dense-4': [ 'dense-32', 'dense-32' ],
    # 'dense-5': [ 'dense-64', 'dense-32' ],
    # # Disused
    # 'dense-6': [ 'dense-128', 'dense-64', 'dense-32' ],
    # 'dense-7': [ 'dense-64', 'dense-64', 'dense-64' ],
    # 'dense-8': [ 'dense-256', 'dense-128', 'dense-64', 'dense-32' ],
    # 'dense-9': [ 'dense-512', 'dense-256', 'dense-128', 'dense-64', 'dense-32' ],
    # 'dense-10': [ 'dense-1024', 'dense-512', 'dense-256', 'dense-128', 'dense-64', 'dense-32' ],
    # GRU
    'gru-1': [ 'gru-16-0-0' ],
    # 'gru-2': [ 'gru-32-0-0' ],
    # 'gru-3': [ 'gru-64-0-0' ],
    # LSTM
    'lstm-1': [ 'lstm-16-0-0' ],
    # 'lstm-2': [ 'lstm-32-0-0' ],
    # 'lstm-3': [ 'lstm-64-0-0' ],
    # 'lstm-4': [ 'lstm-128-0-0' ],
}

search_space = {
    'activation': [ 'linear', 'relu', 'sigmoid', 'tanh' ],
    # 'activation': [ 'linear', 'relu', ],
    # 'activation': [ 'linear', ],
    'final_activation': [ 'linear', 'relu', ],
    # 'final_activation': [ 'linear', ],
    # 'kernel_size': [ 2, 4, 8 ],
    'kernel_size': [ 2, 4, 6, 8, 10, 12 ],
    # 'learning_rate': [ 1, 0.1, 0.01, 0.001, 0.0001 ],
    'layer_1_size': [ 16, 32, 64, 128 ], # earliest layer
    # 'layer_1_size': [ 64, 128, 256, 512 ], # earliest layer
    'layer_2_size': [ 16, 32, 64, 128 ],
    'layer_3_size': [ 16, 32, 64, 128 ], # last layer
    'loss': [ 'mean_absolute_error', 'mean_squared_error' ],
    # 'loss': [ 'mean_absolute_error' ],
    'num_epochs': [ 5, 10, 15, 20, 25 ],
    # 'num_epochs': [ 20, 30, 40, 50, 60, 70 ],
    # 'num_epochs': [ 20, 30, 40, 50, 60 ],
    # 'num_epochs': [ 40, 50, 60, 70, 80 ],
    # 'optimizer': [ 'adadelta', 'adam', 'rmsprop', 'sgd', ],
    'optimizer': [ 'adam', 'rmsprop', 'sgd', ],
    # 'optimizer': [ 'sgd' ],
    # 'patience': [ 1, 3, 5, 7, 9 ],
    # 'min_delta': [ 10, 50, 100, 500 ],
    # 'primary_cap_n_channels': [ 4, 8, 16, 32 ],
    'primary_cap_n_channels': [ 16, 32, 64, 128 ],
    'primary_cap_dim_capsule': [ 4, 8, 16, 32 ],
    # 'primary_cap_dim_capsule': [ 8, ],
    'out_cap_output_dim': [ 4, 8, 16, 32 ],
    # 'out_cap_output_dim': [ 16 ],
    'out_cap_output_num': [ 4, 8, 16, 32 ],
    # 'out_cap_output_num': [ 2, 4, 6, 8, 10 ],
    'routings': [ 2, 3, 4 ],
    # 'routings': [ 3, 4, 5, 6, 7 ],
}

class ModelHandler():
    """Exists as a wrapper for Keras models"""

    def __init__(self, model, architecture, **kwargs):
        self.params = { 
            'verbosity': c.KERAS_LEVEL
        }
        self.model = model
        self.params['architecture'] = architecture
        self.params['batch_size'] = kwargs.get('batch_size', 64)
        self.params['loss'] = kwargs['loss']
        self.params['num_epochs'] = kwargs['num_epochs']
        self.params['optimizer'] = kwargs['optimizer']

    
    @classmethod
    def build_model(cls, input_dim, arch='dense-0', output_shape=1, **kwargs):
        """Create an instance of ModelHandler.
        
        :param cls: (ModelHandler)
        :param arch: neural network architecture to use
        :param kwargs: keyword arguments
        :returns: a compiled and untrained Keras model
        """

        # Reshape for CapsNets, CNNs, GRUs, LSTMs
        if arch.split('-')[0] in ['capsnet', 'cnn', 'gru', 'lstm']:
            input_dim = (input_dim, 1)

        # Add layers
        model_layers = [ Input(shape=input_dim) ]
        prev_layer = model_layers[0]

        for i, layer_spec in enumerate(architectures[arch]):
            layer = ModelHandler._parse_layer_spec(layer_spec, prev_layer, i, **kwargs)
            prev_layer = layer
            model_layers.append(layer)

        # Add final layer for prediction
        callbacks = []
        # if arch.split('-')[0] != 'capsnet':
        model_layers.append(Dense(output_shape, activation=kwargs['final_activation']))
        # else:
        #     filepath = os.path.join('tmp', 'checkpoints',  arch + '.hdf5')
        #     if os.path.exists(filepath):
        #         os.remove(filepath)

        #     callbacks = [
        #         EarlyStopping(monitor='val_loss', patience=5, min_delta=50, verbose=1, mode='auto'), 
        #         ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='auto'),
        #     ]
        
        model = Sequential(model_layers)
        model.compile(loss=kwargs['loss'], optimizer=kwargs['optimizer'])

        return cls(model, arch, **kwargs)


    @staticmethod
    def _parse_layer_spec(layer_spec, prev_layer, layer_num, **kwargs):
        """Given a layer specification, create a Keras model layer
        
        :param layer_spec: Keras model layer specification
        :returns: model layer
        """

        # activation = kwargs.get('activation', 'relu')
        activation = 'relu'
        
        parts = layer_spec.split('-')
        if parts[0] == 'caps':
            layers = capsnet.CapsuleLayer(
                num_capsule=int(kwargs['out_cap_output_num']),
                dim_capsule=int(kwargs['out_cap_output_dim']),
                routings=int(kwargs['routings']),
                kernel_initializer='glorot_uniform'
                )

        elif parts[0] == 'conv1d':
            layers = Conv1D(
                filters=int(parts[1]), #kwargs[f'layer_{layer_num+1}_size']),
                kernel_size=int(parts[2]), #kwargs['kernel_size']),
                activation=activation
                )

        elif parts[0] == 'dense':
            layers = Dense(int(parts[1]), activation=activation)
        
        elif parts[0] == 'dropout':
            layers = Dropout(float(parts[1]), seed=c.SEED)
        
        elif parts[0] == 'flatten':
            layers = Flatten()

        elif parts[0] == 'gru':
            layers = GRU(
                units=int(parts[1]),#kwargs[f'layer_{layer_num+1}_size']), 
                dropout=float(parts[2]), 
                recurrent_dropout=float(parts[3]), 
                activation=activation
                )

        elif parts[0] == 'length':
            layers = capsnet.Length()
        
        elif parts[0] == 'lstm':
            layers = LSTM(
                units=int(parts[1]),#kwargs[f'layer_{layer_num+1}_size']), 
                dropout=float(parts[2]), 
                recurrent_dropout=float(parts[3]), 
                activation=activation
                )

        elif parts[0] == 'maxpool':
            layers = MaxPooling1D(pool_size=int(parts[1]))

        elif parts[0] == 'primary':
            # PrimaryCap returns a tuple 3 layers
            layers = capsnet.PrimaryCap(
                prev_layer,
                n_channels=int(kwargs['primary_cap_n_channels']),
                dim_capsule=int(kwargs['primary_cap_dim_capsule']),
                kernel_size=int(kwargs['kernel_size']),
                # strides=int(parts[4]),
                # padding=int(parts[5]),
                )

        else:
            raise ValueError('Unexpected layer specification:', layer_spec)
        return layers


    def fit(self, X, y, **kwargs):
        try:
            self._fit_model(X, y, **kwargs)
        except:
            X = np.expand_dims(X, axis=2)
            self._fit_model(X, y, **kwargs)


    def _fit_model(self, X, y, **kwargs):
        val_split = kwargs.get('val_split', 0.0)
        # callbacks = kwargs.get('callbacks', [])
        
        self.model.fit(
            X, y.values, 
            validation_split=val_split,
            batch_size=self.params['batch_size'], 
            # callbacks=callbacks,
            epochs=self.params['num_epochs'], 
            verbose=self.params['verbosity']
            )


    def predict(self, instance):
        try: # FFNNs
            predictions = self.model.predict(instance.values)
        except: # CNNs, RNNs, etc
            instance = np.expand_dims(instance, axis=2)
            predictions = self.model.predict(instance)
        return predictions


    def set_params(self, **kwargs):
        for key, value in kwargs.items():
            self.params[key] = value


    def get_params(self):
        return self.params

