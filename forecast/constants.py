import configparser
from datetime import datetime
import logging
import os

# Default directories
LOG_DIR = 'logs'
CONFIG_DIR = 'config'
DATA_DIR = 'data'
PLOT_DIR = os.path.join(DATA_DIR, 'plots')
RESULTS_DIR = 'results'

for directory in [LOG_DIR, CONFIG_DIR, DATA_DIR, PLOT_DIR, RESULTS_DIR]:
	if not os.path.exists(directory):
		os.makedirs(directory)

# Read default configuration file
config = configparser.ConfigParser()
config_file = os.path.join(CONFIG_DIR, 'config.cfg')
config.read(config_file)

# Parameters from configuration file
# LOGS
KERAS_LEVEL = int(config.get('LOGS', 'keras_level'))
LOG_FILE = config.get('LOGS', 'log_file')
LOG_LEVEL = config.get('LOGS', 'log_level')
SKLEARN_LEVEL = int(config.get('LOGS', 'sklearn_level'))
TF_LEVEL = int(config.get('LOGS', 'tf_level'))
# RUNTIME
NUM_JOBS = int(config.get('RUNTIME', 'num_jobs'))
SEED = int(config.get('RUNTIME', 'seed'))
# TASKS
DATA_FILE = config.get('TASKS', 'data_file')
RESULTS_FILE = config.get('TASKS', 'results_file')
TASK_NUM = int(config.get('TASKS', 'task_num'))
SAVE_PREDICTIONS=config.get('TASKS', 'save_predictions')
SAVE_PREDICTIONS = True if SAVE_PREDICTIONS in ['true', 'True', 'yes', 'y'] else False

# Disable overly verbose logging from matplotlib and ipython
logging.getLogger('parso.python.diff').disabled = True
logging.getLogger('matplotlib.font_manager').disabled = True
logging.getLogger('matplotlib').disabled = True

# Initialise Logger
LOG_PATH = os.path.join(LOG_DIR, LOG_FILE)
logging_format = '%(levelname)s:  %(message)s'
logging.basicConfig(filename=LOG_PATH, filemode='a', level='DEBUG', format=logging_format)
logger = logging.getLogger()


######################################################################
#### OTHER IMPORTS (after logging has been configured)
######################################################################

from numpy.random import seed
from sklearn.dummy import DummyRegressor
from sklearn.ensemble import AdaBoostRegressor, BaggingRegressor, RandomForestRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.linear_model import BayesianRidge, Lasso, Lars, LinearRegression, PassiveAggressiveRegressor, Ridge, \
	SGDRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import LinearSVR, NuSVR, SVR
from sklearn.tree import DecisionTreeRegressor, ExtraTreeRegressor
# from statsmodels.tsa.holtwinters import SimpleExpSmoothing
from statsmodels.tsa.vector_ar.var_model import VAR
from statsmodels.tsa.statespace.varmax import VARMAX
import tensorflow

# Set seed
seed(SEED) # numpy
try: # >= TF 2.*
	tensorflow.random.set_seed(SEED)
except: # < TF 2.*
	tensorflow.compat.v1.set_random_seed(SEED)


######################################################################
#### PRE-PROCESSING PARAMETERS
######################################################################

# Allowed options for pre-processing parameters
preprocessing_params = {
	# 'data_augmentation': [ None ],
	# 'feature_selection': [ None, 'kbest' ],
	# 'kbest': int,
	# 'resampling': [ None, 'randomOverSampling', 'randomUnderSampling'], #'adasyn', 'smote', 'smoteEnn', 'smoteTomek' ],
	'scaling': [ 'minmax', 'normalize', 'standard', 'robust', None ],
}

######################################################################
#### FEATURE SELECTION
######################################################################

feature_selection = {
	'mi': [ 12 ],
	'f_regression': [ 20 ],
	'pearson': [ 20 ],
	'importance': [ 21 ],
}

######################################################################
#### MODELLING PARAMETERS
######################################################################

test_split = 0.1
val_split = 0.1
validate_by_day = True # batch predictions by day during validation

######################################################################
#### SCIKIT-LEARN
######################################################################

# Models with default parameters
# Note: ensure that model names are not subsets of other model names to allow easier file cleaning
sklearn_models = {
	# 'bayesian_ridge': lambda **kwargs: BayesianRidge(normalize=False, verbose=SKLEARN_LEVEL),
	# 'knr': lambda **kwargs: KNeighborsRegressor(algorithm='auto'),
	'linear_regression': lambda **kwargs: LinearRegression(),
	# 'random_forest': lambda **kwargs: RandomForestRegressor(random_state=SEED, verbose=SKLEARN_LEVEL),

	# 'lars': lambda **kwargs: Lars(verbose=SKLEARN_LEVEL), # several orders of magnitude worse than other models
	# 'nu_svr': lambda **kwargs: NuSVR(verbose=SKLEARN_LEVEL, max_iter=100000),
	# 'ridge_regression': lambda **kwargs: Ridge(random_state=SEED),
	# 'std_svr': lambda **kwargs: SVR(verbose=SKLEARN_LEVEL, max_iter=100000),
	# 'gaussian_process': lambda **kwargs: GaussianProcessRegressor(random_state=SEED),
}

unused_sklearn_models = {
	# Shortlisted
	'dummy': lambda **kwargs: DummyRegressor(strategy='mean'), # no more runs needed

	# Removed as underperforming
	# 'decision_tree': lambda **kwargs: DecisionTreeRegressor(random_state=SEED),
	# 'extra_tree': lambda **kwargs: ExtraTreeRegressor(random_state=SEED),
	# 'lasso': lambda **kwargs: Lasso(random_state=SEED),
	# 'linear_svr': lambda **kwargs: LinearSVR(random_state=SEED, verbose=SKLEARN_LEVEL),

	# Extra models
	'bagging_default': lambda **kwargs: BaggingRegressor(base_estimator=kwargs['base_estimator'], random_state=SEED),
	'adaboost_default': lambda **kwargs: AdaBoostRegressor(random_state=SEED),
	'adaboost_bayesian_ridge': lambda **kwargs: AdaBoostRegressor(base_estimator=BayesianRidge(), random_state=SEED),
	'adaboost_decision_tree': lambda **kwargs: AdaBoostRegressor(base_estimator=DecisionTreeRegressor(), random_state=SEED),
	'adaboost_knr': lambda **kwargs: AdaBoostRegressor(base_estimator=KNeighborsRegressor(), random_state=SEED),
	'adaboost_lasso': lambda **kwargs: AdaBoostRegressor(base_estimator=Lasso(), random_state=SEED),
	'adaboost_linear_regression': lambda **kwargs: AdaBoostRegressor(base_estimator=LinearRegression(), random_state=SEED),
	'adaboost_svr': lambda **kwargs: AdaBoostRegressor(base_estimator=SVR(kernel="rbf"), random_state=SEED),
	'passive_aggressive': lambda **kwargs: PassiveAggressiveRegressor(random_state=SEED, verbose=SKLEARN_LEVEL),
	'sgd': lambda **kwargs: SGDRegressor(random_state=SEED, verbose=SKLEARN_LEVEL),
}

all_sklearn_models = {**sklearn_models, **unused_sklearn_models}

# Parameter spaces to search for each model
# Important: during randomized searches, lists with floats are treated as continuous ranges
sklearn_search_space = {
	'adaboost_default': {
		'n_estimators': [10, 50, 100, 150, 200, 250, 300],
		'learning_rate': [0.1, 1.0, 10],
		'loss': ['linear', 'square', 'exponential'],
	},
	'bagging_default': {
		'base_estimator': [BayesianRidge, KNeighborsRegressor, DecisionTreeRegressor, Lasso, LinearRegression],
		'n_estimators': [10, 50, 100, 150, 200, 250, 300],
		'learning_rate': [0.1, 1.0, 10],
		'loss': ['linear', 'square', 'exponential'],
	},
	'bayesian_ridge': {
		'n_iter': [150, 300, 450],
		'tol': [1e-2, 1e-3, 1e-4],
		'alpha_1': [1e-5, 1e-6, 1e-7],
		'alpha_2': [1e-5, 1e-6, 1e-7],
		'lambda_1': [1e-5, 1e-6, 1e-7],
		'lambda_2': [1e-5, 1e-6, 1e-7],
	},
	'decision_tree': {
		# 'criterion': ['mse', 'friedman_mse', 'mae'],
		'criterion': ['friedman_mse', 'mae'],
		# 'splitter': ['best', 'random'], # original
		'splitter': ['random'],
		# 'max_depth': [10, 100, 1000, None], # original
		# 'max_depth': [5, 10, 20, 30, 40], # second iteration
		'max_depth': [10],
		# 'max_features':['auto', 'sqrt', 'log2'],
		'max_features':['log2'],
	},
	'dummy': {},
	'extra_tree': {
		# 'criterion': ['mse', 'friedman_mse', 'mae'],
		'criterion': ['mse'],
		'splitter': ['best', 'random'],
		# 'max_depth': [10, 100, 1000, None], # original
		# 'max_depth': [ 5, 10, 20, 30, 40], # second iteration
		'max_depth': [10],
		# 'max_features':['auto', 'sqrt', 'log2'], # original
		'max_features':['sqrt'],
	},
	'gaussian_process': {
		'alpha': [1e-8, 1e-9, 1e-10, 1e-11, 1e-12],
		'n_restarts_optimizer': [0, 1, 2, 3],
		# 'normalize_y': [True, False], # original
		'normalize_y': [True],
	},
	'knr': {
		# 'n_neighbors': [1, 5, 10, 15, 20, 25], # original
		'n_neighbors': [15, 20, 25, 30, 40],
		# 'weights': ['uniform', 'distance'],
		'weights': ['distance'],
		# 'p': [2, 3, 4],
		'p': [2],
	},
	'lars': {
		'fit_intercept': [True, False],
		'n_nonzero_coefs': [1, 10, 100, 500],
		'normalize': [True, False],
		'precompute': [True, False, 'auto'],
	},
	'lasso': {
		'alpha': [0.2, 0.4, 0.6, 0.8, 1.0],
		'tol': [1e-2, 1e-3, 1e-4],
		'selection': ['random', 'cyclic'],
	},
	'linear_regression': {},
	'linear_svr': {
		'epsilon': [0.0, 0.5, 1.0],
		'tol': [1e-3, 1e-4, 1e-5],
		'C': [0.01, 0.1, 1.0, 10],
		'loss': ['epsilon_insensitive', 'squared_epsilon_insensitive'],
		'intercept_scaling': [0.001, 0.1, 1, 10],
		# 'max_iter': [100, 1000, 10000],
	},
	'nu_svr': {
		'nu': [0.0, 0.5, 1.0],
		'C': [0.01, 0.1, 1.0, 10],
		# 'kernel': ['linear', 'rbf', 'sigmoid'],
		'kernel': ['rbf'],
		# 'degree': [2, 3, 4, 5, 6],
		'degree': [2, 4],
		# 'gamma': ['scale', 'auto'],
		'gamma': ['auto'], 
		'coef0': [0.0, 0.5, 1.0],
		'shrinking': [True, False],
		'tol': [1e-3, 1e-4, 1e-5],
		# 'max_iter': [100, 1000, 10000],
	},
	'passive_aggressive': {
		'C': [0.01, 0.1, 1.0, 10],
		# 'max_iter': [100, 1000, 10000],
		'loss': ['epsilon_insensitive', 'squared_epsilon_insensitive'],
		'epsilon': [0.001, 0.01, 0.1, 1.0],
		'average': [True, False, 10],
	},
	'ridge_regression': {
		'alpha': [0.2, 0.4, 0.6, 0.8, 1.0],
		# 'max_iter': [100, 1000, 10000],
		'tol': [1e-2, 1e-3, 1e-4],
		# 'solver': ['auto', 'svd', 'cholesky', 'lsqr', 'sparse_cg', 'sag', 'saga'],
		'solver': ['auto', 'lsqr',],
	},
	'random_forest': {
		# 'n_estimators': [50, 100, 200, 300, 400],
		# 'n_estimators': [50, 100, 200, 300],
		'n_estimators': [100],
		'criterion': ['mae', 'mse'],
		# 'max_depth': [10, 100, 1000, None],
		# 'max_depth': [5, 10, 20, 30, 40],
		'max_depth': [10],
		# 'max_features':['auto', 'sqrt', 'log2'],
		'max_features':['sqrt', 'log2'],
		# 'bootstrap': [True, False],
		'bootstrap': [True],
	},
	'sgd': {
		'loss': ['squared_loss', 'huber', 'epsilon_insensitive', 'squared_epsilon_insensitive'],
		'penalty': ['l2', 'l1', 'elasticnet'],
		'alpha': [1e-3, 1e-4, 1e-5],
		'l1_ratio': [0.0, 0.15, 0.5, 1.0],
		'max_iter': [100, 1000, 10000],
		'tol': [1e-2, 1e-3, 1e-4],
		'epsilon': [0.001, 0.01, 0.1, 1.0],
		'learning_rate': ['constant', 'optimal', 'invscaling', 'adaptive'],
		'early_stopping': [True, False], 
	},
	'std_svr': {
		# 'kernel': ['linear', 'poly', 'rbf', 'sigmoid'],
		'kernel': ['rbf'],
		'degree': [2, 3, 4, 5, 6],
		'gamma': ['scale', 'auto'], 
		'coef0': [0.0, 0.5, 1.0],
		'tol': [1e-3, 1e-4, 1e-5],
		'C': [0.01, 0.1, 1.0, 10, 100],
		'epsilon': [0.0, 0.5, 1.0],
		'shrinking': [True, False],
	},
}

for model_name in all_sklearn_models.keys():
	if model_name.split('_')[0] == 'adaboost':
		sklearn_search_space[model_name] = sklearn_search_space['adaboost_default'].copy()

######################################################################
#### STATSMODELS
######################################################################

stats_models = {
	# error 'ar': lambda **kwargs: AutoReg(kwargs['data'], lags=52, seasonal=True, period=52, trend='n'),
	'arima': lambda **kwargs: ARIMA(kwargs['data'], order=(52, 0, 0)),
	# todo 'arma': lambda **kwargs: ARMA(kwargs['data'], order=(52, 0)),
	# ? 'exponential_smoothing': lambda **kwargs: ExponentialSmoothing(kwargs['data'], trend=None, seasonal='add', seasonal_periods=52),
	# ? 'holt': lambda **kwargs: Holt(kwargs['data']),
	# todo 'sarimax': lambda **kwargs: SARIMAX(kwargs['data'], order=(52, 0, 0)),
	# ? 'simple_exp_smoothing': lambda **kwargs: SimpleExpSmoothing(kwargs['data']), # does not consider seasonality
	# error 'svar': lambda **kwargs: SVAR(kwargs['data'], svar_type='A'),
	# done 'var': lambda **kwargs: VAR(kwargs['data']),
	# error 'varmax': lambda **kwargs: VARMAX(kwargs['data']),
	# error 'vecm': lambda **kwargs: VECM(kwargs['data']),
}

######################################################################
#### KERAS
######################################################################

# Keras architectures. Formats:
# 'dense-32': a densely connected layer with 32 neurons
# 'dropout-0.5': a dropout layer (rate: 0.5)
# 'caps-1-16-3': a capsule layer with output shape 1, 16 capsules and 3 routing-by-agreement iterations
# 'conv1d-64-2': a 1D convolutional layer with 64 filters and kernel size 2
# 'gru-16-0-0': a GRU unit with 16 neurons in dense layers, dropout 0, recurrent dropout 0
# 'lstm-16-0-0': a LSTM unit with 16 neurons in dense layers, dropout 0, recurrent dropout 0
# 'maxpool-2': a max pooling with kernel size 2
# 'primary-32-8-2': a primary capsule layer containing 32 capsules of 8 convolutional filters with kernel size 2
architectures = {
	# CapsNet
	# 'capsnet-1': [ 'conv1d-32-2', 'conv1d-32-2', 'primary-128-8-2', 'caps-1-16-3', 'length' ], # Y. Kim
	# 'capsnet-2': [ 'conv1d-64-2', 'conv1d-32-2', 'primary-32-8-2', 'caps-8-16-3', 'length', 'dense-8' ],
	# 'capsnet-3': [ 'conv1d-32-2', 'primary-32-8-2', 'caps-8-16-3', 'length', 'dense-8' ],
	'capsnet-4': [ 'conv1d-64-2', 'primary-32-8-2', 'caps-8-16-3', 'length' ],
	# 'capsnet-5': [ 'conv1d-64-2', 'conv1d-32-2', 'primary-32-8-2', 'caps-8-16-3', 'length' ], 
	# CNN
	# 'cnn-1d-1': [ 'conv1d-64-2', 'maxpool-2', 'flatten', 'dense-64' ],
	# 'cnn-1d-2': [ 'conv1d-32-2', 'maxpool-2', 'flatten', 'dense-32'  ],
	# 'cnn-1d-3': [ 'conv1d-64-2', 'conv1d-32-2', 'maxpool-2', 'flatten', 'dense-32' ],
	# 'cnn-1d-4': [ 'conv1d-64-2', 'conv1d-32-2', 'maxpool-2', 'flatten', 'dense-64', 'dense-32' ],
	# 'cnn-1d-5': [ 'conv1d-128-2', 'conv1d-64-2', 'conv1d-32-2', 'maxpool-2', 'flatten', 'dense-64', 'dense-32' ],
	# 'cnn-1d-6': [ 'conv1d-32-2', 'maxpool-2', 'flatten' ],
	# 'cnn-1d-7': [ 'conv1d-64-2', 'maxpool-2', 'flatten', ],
	# 'cnn-1d-8': [ 'conv1d-32-2', 'conv1d-32-2', 'maxpool-2', 'flatten', 'dense-16' ],
	# 'cnn-1d-9': [ 'conv1d-64-2', 'conv1d-32-2', 'maxpool-2', 'flatten', 'dense-16' ],
	# FFNN
	# 'dense-0': [ ],
	# 'dense-1': [ 'dense-16' ],
	# 'dense-2': [ 'dense-32' ],
	# 'dense-3': [ 'dense-32', 'dense-16' ],
	# 'dense-4': [ 'dense-32', 'dense-32' ],
	# 'dense-5': [ 'dense-64', 'dense-32' ],
	# # Disused
	# 'dense-6': [ 'dense-128', 'dense-64', 'dense-32' ],
	# # 'dense-7': [ 'dense-64', 'dense-64', 'dense-64' ],
	# # 'dense-8': [ 'dense-256', 'dense-128', 'dense-64', 'dense-32' ],
	# # 'dense-9': [ 'dense-512', 'dense-256', 'dense-128', 'dense-64', 'dense-32' ],
	# # 'dense-10': [ 'dense-1024', 'dense-512', 'dense-256', 'dense-128', 'dense-64', 'dense-32' ],
	# GRU
	# 'gru-1': [ 'gru-16-0-0' ],
	# # 'gru-2': [ 'gru-32-0-0' ],
	# # 'gru-3': [ 'gru-64-0-0' ],
	# LSTM
	# 'lstm-1': [ 'lstm-16-0-0' ],
	# # 'lstm-2': [ 'lstm-32-0-0' ],
	# # 'lstm-3': [ 'lstm-64-0-0' ],
	# # 'lstm-4': [ 'lstm-128-0-0' ],
}

keras_search_space = {
	'default': {
		'activation': [ 'linear', 'relu', 'sigmoid', 'tanh' ],
		'final_activation': [ 'linear', 'relu', ],
		'kernel_size': [ 2, 4, 6, 8, 10, 12 ],
		'learning_rate': [ 1, 0.1, 0.01, 0.001, 0.0001 ],
		'layer_1_size': [ 16, 32, 64, 128 ], # earliest layer
		'layer_2_size': [ 16, 32, 64, 128 ],
		'layer_3_size': [ 16, 32, 64, 128 ], # last layer
		'loss': [ 'hinge', 'mean_absolute_error', 'mean_squared_error' ],
		'num_epochs': [ 1, 5, 10, 15, 20 ],
		'optimizer': [ 'adadelta', 'adam', 'rmsprop', 'sgd', ],
		'patience': [ 1, 3, 5, 7, 9 ],
		'min_delta': [ 10, 50, 100, 500 ],
		'primary_cap_n_channels': [ 4, 8, 16, 32 ],
		'primary_cap_dim_capsule': [ 4, 8, 16, 32 ],
		'out_cap_output_dim': [ 4, 8, 16, 32 ],
		'out_cap_output_num': [ 4, 8, 16, 32 ],
		'routings': [ 3, 4, 5, 6, 7 ],
	},
}

for arch in architectures.keys():
	keras_search_space[arch] = keras_search_space['default'].copy()

# Overwrite default parameters based on experimentation
# capsnet-4
keras_search_space['capsnet-4'].update(
	activation=[ 'linear', 'relu' ],
	layer_1_size=[ 64, 128, 256, 512 ],
	loss=[ 'mean_absolute_error' ],
	num_epochs=[ 40, 50, 60, 70, 80 ],
	optimizer=[ 'sgd' ],
	primary_cap_n_channels=[ 16, 32, 64, 128 ],
	primary_cap_dim_capsule=[ 8 ],
	out_cap_output_dim=[ 16 ],
	out_cap_output_num=[ 2, 4, 6, 8, 10 ],
)

