Directory to store log files. Files with .log extension will be ignored by git


Commands to watch log file:

- Unix: tail -f logs/log.log
- Powershell: Get-Content -Path "\.logs\log.log" -Wait